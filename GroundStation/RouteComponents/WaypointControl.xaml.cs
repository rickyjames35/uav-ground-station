﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GroundStation.RouteComponents
{
    /// <summary>
    /// Interaction logic for WaypointControl.xaml
    /// </summary>
    public partial class WaypointControl : UserControl
    {
        public string WaypointName
        {
            set { lblWaypointName.Content = value; }
        }

        public WaypointControl(string wpName)
        {
            InitializeComponent();
            WaypointName = wpName;            
        }

        public void SetActive()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                lblWaypointName.Background = (Brush)new BrushConverter().ConvertFrom("#7F00FF00");
            }));
        }

        public void SetInactive()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                lblWaypointName.Background = (Brush)new BrushConverter().ConvertFrom("#7F00AEFF");
            }));
        }
    }
}
