﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GroundStation
{
    /// <summary>
    /// Interaction logic for VideoFeed.xaml
    /// </summary>
    public partial class VideoFeedWindow : Window
    {
        HUD_Window HUD_Window_;
        ProcessStartInfo psi_;
        Process ps_;

        public VideoFeedWindow()
        {
            InitializeComponent();
            HUD_Window_ = new HUD_Window();
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {            
            HUD_Window_.Owner = this;
            HUD_Window_.Show();

            HwndSource source = (HwndSource)HwndSource.FromVisual(this);
            TextWriter batFile = new StreamWriter("videoFeed.bat");
            batFile.WriteLine("nc.exe -L -p 5002 | mplayer.exe -wid " + source.Handle + " -nofs -noquiet -identify -slave -nomouseinput -sub-fuzziness 1 -vo direct3d, -ao dsound -fps 60 -cache 1024 -");            
            batFile.Close();
            psi_ = new ProcessStartInfo("videoFeed.bat");
            psi_.WindowStyle = ProcessWindowStyle.Minimized;
            ps_ = Process.Start(psi_);

            MemoryStructs.StartStopVideoStruct startStopVideo = new MemoryStructs.StartStopVideoStruct();
            startStopVideo.start = true;
            GS_Comm.GetInstance().SendData(MemoryStructs.MemoryStruct_List.START_STOP_VIDEO, MemoryStructs.StructToByteArray<MemoryStructs.StartStopVideoStruct>(startStopVideo));

            HUD_Window_.Owner = this;
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            HUD_Window_.Close();

            try
            {
                ps_.CloseMainWindow();
                ps_.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            MemoryStructs.StartStopVideoStruct startStopVideo = new MemoryStructs.StartStopVideoStruct();
            startStopVideo.start = false;
            GS_Comm.GetInstance().SendData(MemoryStructs.MemoryStruct_List.START_STOP_VIDEO, MemoryStructs.StructToByteArray<MemoryStructs.StartStopVideoStruct>(startStopVideo));
        }

        private void Window_SizeChanged_1(object sender, SizeChangedEventArgs e)
        {           
            FormatHUD();
        }

        private void Window_LocationChanged_1(object sender, EventArgs e)
        {
            FormatHUD();
        }

        private void FormatHUD()
        {
            Point locationFromWindow = mainCanvas.TranslatePoint(new Point(0, 0), this);
            Point locationFromScreen = mainCanvas.PointToScreen(locationFromWindow);
            HUD_Window_.Top = locationFromScreen.Y;
            HUD_Window_.Left = locationFromScreen.X;
            HUD_Window_.Width = mainCanvas.ActualWidth;
            HUD_Window_.Height = mainCanvas.ActualHeight;
        }
     
    }
}


