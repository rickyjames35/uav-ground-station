﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GroundStation
{
    /// <summary>
    /// Interaction logic for HILSimUC.xaml
    /// </summary>
    public partial class HILSimUC : UserControl
    {
        private const int UPDATE_RATE = 100;
        Thread mainThread;
        bool running;
        UdpClient udpClient;
        IPEndPoint RemoteIpEndPointReceive = new IPEndPoint(IPAddress.Any, 49001);
        IPEndPoint RemoteIpEndPointSend = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 49000);

        public HILSimUC()
        {
            InitializeComponent();
            running = false;
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            udpClient = new UdpClient(RemoteIpEndPointReceive);
            GS_Comm.GetInstance().OnHIL_InputReceived += HILSimUC_OnHIL_InputReceived;
        }

        void HILSimUC_OnHIL_InputReceived(object sender, GS_Comm.HIL_InputArgs e)
        {
            string inputString = e.hilInput_.flaps.ToString() + "," +
                                    e.hilInput_.ailerons.ToString() + "," +
                                    e.hilInput_.elevators.ToString() + "," +
                                    e.hilInput_.rudder.ToString() + "," +
                                    e.hilInput_.throttle.ToString() + "," +
                                    "1" + "\n"; //starter
            byte[] inputArray = Encoding.ASCII.GetBytes(inputString);
            udpClient.Send(inputArray, inputArray.Length, RemoteIpEndPointSend);
        }

        private void chkHILEnabled_Click(object sender, RoutedEventArgs e)
        {
            if (chkHILEnabled.IsChecked.Value)
            {
                running = true;
                mainThread = new Thread(Update);
                mainThread.Start();
            }
            else
            {
                running = false;
            }

            MemoryStructs.HIL_SettingsStruct hilSettingsStruct = new MemoryStructs.HIL_SettingsStruct();
            hilSettingsStruct.hilEnabled = running;
            GS_Comm.GetInstance().SendData(MemoryStructs.MemoryStruct_List.HIL_SETTINGS, MemoryStructs.StructToByteArray<MemoryStructs.HIL_SettingsStruct>(hilSettingsStruct));
        }        

        private void Update()
        {            
            while (running)
            {
                try
                {
                    Byte[] receiveBytes = udpClient.Receive(ref RemoteIpEndPointReceive);
                    Dispatcher.BeginInvoke(new Action(() => { lblDataReceive.Content = "Last Data Receive: " + DateTime.Now.ToLongTimeString(); }));

                    string[] data = Encoding.ASCII.GetString(receiveBytes).Split(',');

                    MemoryStructs.HIL_OutputStruct hilOutputStruct = new MemoryStructs.HIL_OutputStruct();
                    hilOutputStruct.latitude = (float)Convert.ToDouble(data[0]);
                    hilOutputStruct.longitude = (float)Convert.ToDouble(data[1]);
                    hilOutputStruct.altitude = (float)Convert.ToDouble(data[2]);
                    hilOutputStruct.RALT = hilOutputStruct.altitude - (float)Convert.ToDouble(data[3]);
                    hilOutputStruct.airspeed = (float)Convert.ToDouble(data[4]);
                    hilOutputStruct.yaw = (float)Convert.ToDouble(data[5]);
                    hilOutputStruct.pitch = (float)Convert.ToDouble(data[6]);
                    hilOutputStruct.roll = (float)Convert.ToDouble(data[7]);
                    GS_Comm.GetInstance().SendData(MemoryStructs.MemoryStruct_List.HIL_OUTPUT, MemoryStructs.StructToByteArray<MemoryStructs.HIL_OutputStruct>(hilOutputStruct));
                    Thread.Sleep(10);
                }
                catch
                {
                    running = false;
                }
                
            }
            //Thread.Sleep(UPDATE_RATE);
        }

        private void UserControl_Unloaded_1(object sender, RoutedEventArgs e)
        {
            udpClient.Close();
        }
    }
}
