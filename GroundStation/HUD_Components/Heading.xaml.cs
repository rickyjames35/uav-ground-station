﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GroundStation.HUD_Components
{   
    /// <summary>
    /// Interaction logic for Heading.xaml
    /// </summary>
    public partial class Heading : UserControl
    {
        private const float lineWidth = 2F; //pixel
        private const float bigTickHeightScale = 0.2F;
        private const float littleTickHeightScale = 0.1F;
        private const float bottomLinePosition = 0.8F;
        private const float visibleDegrees = 100.0F;

        Rectangle bottomLine;
        Rectangle[] bigTicks;
        Rectangle[] littleTicks;
        Label[] marks;

        float heading_;

        public Heading()
        {
            InitializeComponent();
            bottomLine = new Rectangle();
            bottomLine.Fill = System.Windows.Media.Brushes.LimeGreen;
            bottomLine.Height = lineWidth;
            bottomLine.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            bottomLine.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            Grid1.Children.Add(bottomLine);

            marks = new Label[12];
            for (int i = 0; i < marks.Length; i++)
            {
                marks[i] = new Label();
                marks[i].Foreground = System.Windows.Media.Brushes.LimeGreen;
                marks[i].HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                marks[i].VerticalAlignment = System.Windows.VerticalAlignment.Top;
                if (i == 0)
                    marks[i].Content = "N";
                else if (i == 3)
                    marks[i].Content = "E";
                else if (i == 6)
                    marks[i].Content = "S";
                else if (i == 9)
                    marks[i].Content = "W";
                else
                    marks[i].Content = i * 3;

                Grid1.Children.Add(marks[i]);
            }

            bigTicks = new Rectangle[36];
            for (int i = 0; i < bigTicks.Length; i++)
            {
                bigTicks[i] = new Rectangle();
                bigTicks[i].Fill = System.Windows.Media.Brushes.LimeGreen;
                bigTicks[i].Width = lineWidth;
                bigTicks[i].HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                bigTicks[i].VerticalAlignment = System.Windows.VerticalAlignment.Top;
                Grid1.Children.Add(bigTicks[i]);
            }

            littleTicks = new Rectangle[36];
            for (int i = 0; i < littleTicks.Length; i++)
            {
                littleTicks[i] = new Rectangle();
                littleTicks[i].Fill = System.Windows.Media.Brushes.LimeGreen;
                littleTicks[i].Width = lineWidth;
                littleTicks[i].HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                littleTicks[i].VerticalAlignment = System.Windows.VerticalAlignment.Top;
                Grid1.Children.Add(littleTicks[i]);
            }

            heading_ = 0;            
        }

        private void Grid1_Loaded(object sender, RoutedEventArgs e)
        {
            Resize();
        }

        private void Grid1_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Resize();
        }

        public void SetHeading(float heading)
        {
            heading_ = heading;
            heading = heading + (visibleDegrees / 2);
            if (heading >= 360)            
                heading = heading - 360;

            float spacing = (float)this.Width / (visibleDegrees / 10.0F);
            float offSet = (float)this.Width - ((heading / 10.0F) * spacing);
            for (int i = 0; i < bigTicks.Length; i++)
            {
                float left = (i * spacing) + offSet;
                if (left > this.Width)
                    left = (left - (spacing * 36));

                bigTicks[i].Margin = new Thickness(left, bottomLine.Margin.Top - (bigTicks[i].Height / 2) + 1, 0, 0);
                littleTicks[i].Margin = new Thickness(left + (spacing / 2), bottomLine.Margin.Top - (littleTicks[i].Height / 2) + 1, 0, 0);                
                if (i % 3 == 0)                
                    marks[i / 3].Margin = new Thickness(left - (marks[i / 3].ActualWidth / 2) + 1, bigTicks[i].Margin.Top - marks[i / 3].ActualHeight, 0, 0);                                
            }
        }

        private void Resize()
        {
            bottomLine.Width = this.Width;
            bottomLine.Margin = new Thickness(0, this.Height * bottomLinePosition, 0, 0);            

            for (int i = 0; i < bigTicks.Length; i++)
            {
                bigTicks[i].Height = (this.Height * bigTickHeightScale) + 1;
            }            

            for (int i = 0; i < littleTicks.Length; i++)
                littleTicks[i].Height = (this.Height * littleTickHeightScale) + 1;

            SetHeading(heading_);
        }
    }
}
