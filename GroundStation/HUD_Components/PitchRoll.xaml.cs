﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GroundStation.HUD_Components
{
    /// <summary>
    /// Interaction logic for PitchRoll.xaml
    /// </summary>
    public partial class PitchRoll : UserControl
    {
        private const float lineWidth = 2F; //pixel
        private const float visibleDegrees = 30.0F;
        private const float horizontalLineWidth = 0.12F;
        private const float verticalLineWidth = 0.02F;        

        Rectangle[] leftHorizontal;
        Rectangle[] leftVertical;
        Rectangle[] rightHorizontal;
        Rectangle[] rightVertical;
        Rectangle horizon;
        Label[] leftMarks;
        Label[] rightMarks;

        float pitch_;
        float roll_;

        public PitchRoll()
        {
            InitializeComponent();
            leftHorizontal = new Rectangle[36];
            leftVertical = new Rectangle[36];
            rightHorizontal = new Rectangle[36];
            rightVertical = new Rectangle[36];
            leftMarks = new Label[36];
            rightMarks = new Label[36];
            horizon = new Rectangle();
            horizon.Fill = System.Windows.Media.Brushes.LimeGreen;
            horizon.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            horizon.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            horizon.Height = lineWidth;            
            Grid1.Children.Add(horizon);
            int startDegree = 90;

            for (int i = 0; i < 36; i++)
            {
                leftMarks[i] = new Label();
                leftMarks[i].Content = startDegree.ToString();
                leftMarks[i].Foreground = System.Windows.Media.Brushes.LimeGreen;
                leftMarks[i].HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                leftMarks[i].VerticalAlignment = System.Windows.VerticalAlignment.Top;
                Grid1.Children.Add(leftMarks[i]);

                rightMarks[i] = new Label();
                rightMarks[i].Content = startDegree.ToString();
                rightMarks[i].Foreground = System.Windows.Media.Brushes.LimeGreen;
                rightMarks[i].HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                rightMarks[i].VerticalAlignment = System.Windows.VerticalAlignment.Top;
                Grid1.Children.Add(rightMarks[i]);

                if (i == 17)
                    startDegree -= 10;
                else
                    startDegree -= 5;

                leftHorizontal[i] = new Rectangle();
                leftHorizontal[i].Fill = System.Windows.Media.Brushes.LimeGreen;
                leftHorizontal[i].HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                leftHorizontal[i].VerticalAlignment = System.Windows.VerticalAlignment.Top;
                leftHorizontal[i].Height = lineWidth;
                Grid1.Children.Add(leftHorizontal[i]);

                leftVertical[i] = new Rectangle();
                leftVertical[i].Fill = System.Windows.Media.Brushes.LimeGreen;
                leftVertical[i].HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                leftVertical[i].VerticalAlignment = System.Windows.VerticalAlignment.Top;
                leftVertical[i].Width = lineWidth;
                Grid1.Children.Add(leftVertical[i]);

                rightHorizontal[i] = new Rectangle();
                rightHorizontal[i].Fill = System.Windows.Media.Brushes.LimeGreen;
                rightHorizontal[i].HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                rightHorizontal[i].VerticalAlignment = System.Windows.VerticalAlignment.Top;
                rightHorizontal[i].Height = lineWidth;
                Grid1.Children.Add(rightHorizontal[i]);

                rightVertical[i] = new Rectangle();
                rightVertical[i].Fill = System.Windows.Media.Brushes.LimeGreen;
                rightVertical[i].HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                rightVertical[i].VerticalAlignment = System.Windows.VerticalAlignment.Top;
                rightVertical[i].Width = lineWidth;
                Grid1.Children.Add(rightVertical[i]);
            }
        }

        public void SetPitchRoll(float pitch, float roll)
        {
            pitch_ = pitch;
            roll_ = roll;

            Resize();
        }

        private void Grid_SizeChanged_1(object sender, SizeChangedEventArgs e)
        {
            Resize();
        }

        private void Grid_Loaded_1(object sender, RoutedEventArgs e)
        {
            Resize();
        }

        private void Resize()
        {            
            float spacing = (float)this.Height / (visibleDegrees / 5.0f);
            float offset = ((float)this.Height / visibleDegrees) * pitch_;

            for (int i = 0; i < 18; i++)
            {
                leftHorizontal[i].Width = this.Width * horizontalLineWidth;
                leftHorizontal[i].Margin = new Thickness((this.Width / 2) - (leftHorizontal[i].Width * 2), (this.Height / 2) + offset - ((i + 1) * spacing), 0, 0);

                leftVertical[i].Height = this.Height * verticalLineWidth;
                leftVertical[i].Margin = new Thickness(leftHorizontal[i].Margin.Left, leftHorizontal[i].Margin.Top, 0, 0);

                rightHorizontal[i].Width = this.Width * horizontalLineWidth;
                rightHorizontal[i].Margin = new Thickness((this.Width / 2) + rightHorizontal[i].Width, (this.Height / 2) + offset - ((i + 1) * spacing), 0, 0);

                rightVertical[i].Height = this.Height * verticalLineWidth;
                rightVertical[i].Margin = new Thickness(rightHorizontal[i].Margin.Left + rightHorizontal[i].Width - 1, rightHorizontal[i].Margin.Top, 0, 0);

                leftMarks[17 - i].Margin = new Thickness(leftVertical[i].Margin.Left - leftMarks[17 - i].ActualWidth - 2, leftVertical[i].Margin.Top + (leftVertical[i].Height / 2) - (leftMarks[17 - i].ActualHeight / 2), 0, 0);
                rightMarks[17 - i].Margin = new Thickness(rightVertical[i].Margin.Left + 3, rightVertical[i].Margin.Top + (rightVertical[i].Height / 2) - (rightMarks[17 - i].ActualHeight / 2), 0, 0);

                TransformGroup tgMark = new TransformGroup();
                RotateTransform rtMark = new RotateTransform(-roll_);
                tgMark.Children.Add(rtMark);
                leftMarks[17 - i].RenderTransformOrigin = new Point(0.5, 0.5);
                leftMarks[17 - i].RenderTransform = tgMark;
                rightMarks[17 - i].RenderTransformOrigin = new Point(0.5, 0.5);
                rightMarks[17 - i].RenderTransform = tgMark;
            }      

            horizon.Width = this.Width;
            horizon.Margin = new Thickness(0, (this.Height / 2) + offset, 0, 0);

            for (int i = 0; i < 18; i++)
            {
                leftHorizontal[i + 18].Width = this.Width * horizontalLineWidth;
                leftHorizontal[i + 18].Margin = new Thickness((this.Width / 2) - (leftHorizontal[i + 18].Width * 2), (this.Height / 2) + offset + ((i + 1) * spacing), 0, 0);

                leftVertical[i + 18].Height = this.Height * verticalLineWidth;
                leftVertical[i + 18].Margin = new Thickness(leftHorizontal[i + 18].Margin.Left, leftHorizontal[i + 18].Margin.Top, 0, 0);

                rightHorizontal[i + 18].Width = this.Width * horizontalLineWidth;
                rightHorizontal[i + 18].Margin = new Thickness((this.Width / 2) + rightHorizontal[i + 18].Width, (this.Height / 2) + offset + ((i + 1) * spacing), 0, 0);                

                rightVertical[i + 18].Height = this.Height * verticalLineWidth;
                rightVertical[i + 18].Margin = new Thickness(rightHorizontal[i + 18].Margin.Left + rightHorizontal[i + 18].Width - 1, rightHorizontal[i+ 18].Margin.Top, 0, 0);

                leftMarks[i + 18].Margin = new Thickness(leftVertical[i + 18].Margin.Left - leftMarks[i + 18].ActualWidth - 2, leftVertical[i + 18].Margin.Top + (leftVertical[i + 18].Height / 2) - (leftMarks[i + 18].ActualHeight / 2), 0, 0);
                rightMarks[i + 18].Margin = new Thickness(rightVertical[i + 18].Margin.Left + 3, rightVertical[i + 18].Margin.Top + (rightVertical[i + 18].Height / 2) - (rightMarks[i + 18].ActualHeight / 2), 0, 0);

                TransformGroup tgMark = new TransformGroup();
                RotateTransform rtMark = new RotateTransform(-roll_);
                tgMark.Children.Add(rtMark);
                leftMarks[i + 18].RenderTransformOrigin = new Point(0.5, 0.5);
                leftMarks[i + 18].RenderTransform = tgMark;
                rightMarks[i + 18].RenderTransformOrigin = new Point(0.5, 0.5);
                rightMarks[i + 18].RenderTransform = tgMark;
            }            

            TransformGroup tg = new TransformGroup();
            RotateTransform rt = new RotateTransform(roll_);
            tg.Children.Add(rt);
            Grid1.RenderTransformOrigin = new Point(0.5, 0.5);
            Grid1.RenderTransform = tg;
        }
    }
}
