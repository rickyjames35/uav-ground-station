﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GroundStation.HUD_Components
{
    /// <summary>
    /// Interaction logic for NoseVector.xaml
    /// </summary>
    public partial class NoseVector : UserControl
    {
        private const float lineWidth = 2F; //pixel
        private const float lineLength = 120f / 300f; //percent

        public NoseVector()
        {
            InitializeComponent();
        }

        private void Grid_Loaded_1(object sender, RoutedEventArgs e)
        {
            Resize();
        }

        private void Grid_SizeChanged_1(object sender, SizeChangedEventArgs e)
        {
            Resize();
        }

        public void Resize()
        {
            try
            {
                topLine.Width = lineWidth;
                topLine.Height = lineLength * this.Width;
                topLine.Margin = new Thickness((this.Width / 2) - (topLine.Width / 2), 0, 0, 0);

                leftLine.Width = lineLength * this.Width;
                leftLine.Height = lineWidth;
                leftLine.Margin = new Thickness(0, (this.Width / 2) - (leftLine.Height / 2), 0, 0);

                bottomLine.Width = lineWidth;
                bottomLine.Height = lineLength * this.Width;
                bottomLine.Margin = new Thickness((this.Width / 2) - (bottomLine.Width / 2), this.Height - bottomLine.Height, 0, 0);

                rightLine.Width = lineLength * this.Width;
                rightLine.Height = lineWidth;
                rightLine.Margin = new Thickness(this.Width - rightLine.Width, (this.Width / 2) - (rightLine.Height / 2), 0, 0);
            }
            catch
            {

            }
        }
    }
}
