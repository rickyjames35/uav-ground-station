﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GroundStation
{
    /// <summary>
    /// Interaction logic for GPS_StatusUC.xaml
    /// </summary>
    public partial class GPS_StatusUC : UserControl
    {
        public GPS_StatusUC()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            GS_Comm.GetInstance().OnGpsOutputReceived += GPS_StatusUC_OnGpsOutputReceived;
        }

        void GPS_StatusUC_OnGpsOutputReceived(object sender, GS_Comm.GpsOutputArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                txtUavLatitude.Text = e.gpsOutput_.latitude.ToString();
                txtUavLongitude.Text = e.gpsOutput_.longitude.ToString();
                txtAltitude.Text = e.gpsOutput_.altitude.ToString();
                txtKnots.Text = e.gpsOutput_.knots.ToString();
            }));
        }
    }
}
