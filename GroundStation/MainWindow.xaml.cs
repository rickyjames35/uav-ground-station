﻿using GroundStation.HID_Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using GMap.NET.WindowsPresentation;
using GMap.NET.MapProviders;
using GMap.NET;
using Xceed.Wpf.AvalonDock.Layout;
using System.IO;
using Xceed.Wpf.AvalonDock.Layout.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace GroundStation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        VideoFeedWindow videoFeedWindow;
        const int DEFAULT_FLOAT_X = 100;
        const int DEFAULT_FLOAT_Y = 100;
        const int DOCK_EXTRA_WIDTH = 20;
        const int DOCK_EXTRA_HEIGHT = 40;
        const int RECONNECT_RATE = 2000;
        const int UPDATE_RATE = 10;

        GMapMarker uavMarker_;
        Image uavImage_;
        GMapControl MainMap_;
        ConnectionSettingsUC.ConnectArgs connectionInfo_;
        DateTime lastConnectionAttempt_;
        RouteHelper routeHelper_;

        public MainWindow()
        {            
            InitializeComponent();            
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            routeHelper_ = new RouteHelper();
            lastConnectionAttempt_ = DateTime.Now;
            XBoxInput.GetInstance().Start(new WindowInteropHelper(this).Handle);
            ControlManager.GetInstance().Start();
            LoadLayout(true);
            GS_Comm.GetInstance().OnGpsOutputReceived += MainWindow_OnGpsOutputReceived;
            GS_Comm.GetInstance().OnYawPitchRollReceived += MainWindow_OnYawPitchRollReceived;
            this.Activate();
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (videoFeedWindow != null && videoFeedWindow.IsLoaded)
                videoFeedWindow.Close();

            GS_Comm.GetInstance().Stop();
            XBoxInput.GetInstance().Stop();
            ControlManager.GetInstance().Stop();
        }

        #region Methods

        private void LoadMap()
        {
            BitmapImage uavBmp = new BitmapImage();
            uavBmp.BeginInit();
            uavBmp.UriSource = new Uri("pack://application:,,,/GroundStation;component/Resources/uav.png");
            uavBmp.EndInit();
            uavImage_ = new Image();
            uavImage_.Source = uavBmp;
            uavMarker_ = new GMapMarker();
            uavMarker_.Shape = uavImage_;
            uavMarker_.Offset = new Point(-uavImage_.Source.Width / 2, -uavImage_.Source.Height / 2);
            MainMap_.Markers.Add(uavMarker_);
            MainMap_.MapProvider = GMapProviders.BingHybridMap;
            MainMap_.Position = new PointLatLng(38.78907, -90.32354);
            MainMap_.MinZoom = 1;
            MainMap_.MaxZoom = 19;
            MainMap_.Zoom = 19;
            routeHelper_.SetUAV(ref uavMarker_);
        }

        //fix this it's getting called too many times
        private void PopulateLayouts(bool autoConnect)
        {
            foreach (LayoutAnchorable item in mainDockManager.Layout.Descendents().OfType<LayoutAnchorable>())
            {
                if (item.Title == HID_InputMenu.Header.ToString())
                    item.Content = CreateHID_InputWindowUC();
                else if (item.Title == ConnectionSettingsMenu.Header.ToString())
                    item.Content = CreateConnectionSettingsUC(autoConnect);
                else if (item.Title == ConnectionStatusMenu.Header.ToString())
                    item.Content = CreateConnectionStatusUC();
                else if (item.Title == GPS_StatusMenu.Header.ToString())
                    item.Content = CreateGPS_StatusUC();
                else if (item.Title == HIL_Menu.Header.ToString())
                    item.Content = CreateHIL_SimUC();
                else if (item.Title == AutopilotMenu.Header.ToString())
                    item.Content = CreateAutopilotUC();
                else if (item.Title == ConsoleMenu.Header.ToString())
                    item.Content = CreateConsoleUC();
            }

            foreach (LayoutDocument item in mainDockManager.Layout.Descendents().OfType<LayoutDocument>())
            {
                if (item.Title == MapMenu.Header.ToString())
                    item.Content = CreateGMapControl();
            }
        }

        private HID_InputWindowUC CreateHID_InputWindowUC()
        {
            HID_InputWindowUC hidInputWindowUC = new HID_InputWindowUC();
            hidInputWindowUC.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            hidInputWindowUC.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            return hidInputWindowUC;
        }

        private ConnectionSettingsUC CreateConnectionSettingsUC(bool autoConnect)
        {
            ConnectionSettingsUC connectionSettingsUC = new ConnectionSettingsUC(autoConnect);
            connectionSettingsUC.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            connectionSettingsUC.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            connectionSettingsUC.OnAttemptConnect += connectionSettingsUC_OnAttemptConnect;
            return connectionSettingsUC;
        }

        private ConnectionStatusUC CreateConnectionStatusUC()
        {
            ConnectionStatusUC connectionStatusUC = new ConnectionStatusUC();
            connectionStatusUC.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            connectionStatusUC.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            return connectionStatusUC;
        }

        private GPS_StatusUC CreateGPS_StatusUC()
        {
            GPS_StatusUC gpsStatusUC = new GPS_StatusUC();
            gpsStatusUC.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            gpsStatusUC.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            return gpsStatusUC;
        }

        private HILSimUC CreateHIL_SimUC()
        {
            HILSimUC hilSimUC = new HILSimUC();
            hilSimUC.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            hilSimUC.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            return hilSimUC;
        }

        private GMapControl CreateGMapControl()
        {
            MainMap_ = new GMapControl();
            MainMap_.IgnoreMarkerOnMouseWheel = true;
            MainMap_.MouseRightButtonDown += MainMap__MouseRightButtonDown;
            MainMap_.MouseMove += MainMap__MouseMove;
            MainMap_.MouseLeftButtonDown += MainMap__MouseLeftButtonDown;
            MainMap_.DragButton = MouseButton.Middle;
            MainMap_.MouseWheelZoomType = MouseWheelZoomType.MousePositionWithoutCenter;
            routeHelper_.SetMap(ref MainMap_);
            LoadMap();
            return MainMap_;
        }

        private AutopilotUC CreateAutopilotUC()
        {
            AutopilotUC autopilotUC = new AutopilotUC();
            autopilotUC.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            autopilotUC.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            autopilotUC.SetRouteInfo(ref routeHelper_);
            return autopilotUC;
        }

        private ConsoleUC CreateConsoleUC()
        {
            ConsoleUC consoleUC = new ConsoleUC();
            consoleUC.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            consoleUC.VerticalAlignment = System.Windows.VerticalAlignment.Top;            
            return consoleUC;
        }

        bool WindowExists(string windowTitle)
        {
            foreach (LayoutAnchorable item in mainDockManager.Layout.RootPanel.Descendents().OfType<LayoutAnchorable>())
            {
                if (item.Title == windowTitle)
                    return true;
            }

            foreach (LayoutDocument item in mainDockManager.Layout.RootPanel.Descendents().OfType<LayoutDocument>())
            {
                if (item.Title == windowTitle)
                    return true;
            }

            return false;
        }

        private void SetLayoutDefaults(LayoutAnchorable layout)
        {
            double height = ((UserControl)layout.Content).Height + DOCK_EXTRA_HEIGHT;
            double width = ((UserControl)layout.Content).Width + DOCK_EXTRA_WIDTH;
            layout.AutoHideMinHeight = height;
            layout.AutoHideMinWidth = width;
            layout.AutoHideHeight = height;
            layout.AutoHideWidth = width;
            layout.FloatingHeight = height;
            layout.FloatingWidth = width;
            layout.FloatingTop = DEFAULT_FLOAT_Y;
            layout.FloatingLeft = DEFAULT_FLOAT_X;            
        }

        private void LoadLayout(bool autoConnect)
        {
            var layoutSerializer = new XmlLayoutSerializer(mainDockManager);
            //Here I've implemented the LayoutSerializationCallback just to show
            // a way to feed layout desarialization with content loaded at runtime
            //Actually I could in this case let AvalonDock to attach the contents
            //from current layout using the content ids
            //LayoutSerializationCallback should anyway be handled to attach contents
            //not currently loaded
            layoutSerializer.LayoutSerializationCallback += (s, eArgs) =>
            {
                //if (e.Model.ContentId == FileStatsViewModel.ToolContentId)
                //    e.Content = Workspace.This.FileStats;
                //else if (!string.IsNullOrWhiteSpace(e.Model.ContentId) &&
                //    File.Exists(e.Model.ContentId))
                //    e.Content = Workspace.This.Open(e.Model.ContentId);
            };
            layoutSerializer.Deserialize(@".\AvalonDock.Layout.config");
            PopulateLayouts(autoConnect);
        }

        private void ConnectRaspberryPi()
        {
            GS_Comm.GetInstance().Stop();
            GS_Comm.GetInstance().Start(connectionInfo_.UAV_IP);
            MemoryStructs.ConnectionInfoStruct connectionInfo = new MemoryStructs.ConnectionInfoStruct();
            connectionInfo.firstOctet = Convert.ToByte(connectionInfo_.GroundStationIP.Split(new char[] { '.' })[0]);
            connectionInfo.secondOctet = Convert.ToByte(connectionInfo_.GroundStationIP.Split(new char[] { '.' })[1]);
            connectionInfo.thirdOctet = Convert.ToByte(connectionInfo_.GroundStationIP.Split(new char[] { '.' })[2]);
            connectionInfo.fourthOctet = Convert.ToByte(connectionInfo_.GroundStationIP.Split(new char[] { '.' })[3]);
            GS_Comm.GetInstance().SendData(MemoryStructs.MemoryStruct_List.CONNECTION_INFO, MemoryStructs.StructToByteArray<MemoryStructs.ConnectionInfoStruct>(connectionInfo));
        }

        private void CreateDefaultLayout(string layoutTitle)
        {
            if (!WindowExists(layoutTitle))
            {
                LayoutAnchorable DefaultLayout = new LayoutAnchorable();
                DefaultLayout.Title = layoutTitle;                
                DefaultLayout.AddToLayout(mainDockManager, AnchorableShowStrategy.Left);
                PopulateLayouts(false);
                SetLayoutDefaults(DefaultLayout);
            }
        }

        #endregion

        #region Events

        #region Menu Items

        private void MenuItemVideoFeed_Click(object sender, RoutedEventArgs e)
        {
            if (videoFeedWindow == null || !videoFeedWindow.IsLoaded)
                videoFeedWindow = new VideoFeedWindow();

            videoFeedWindow.Owner = this;
            videoFeedWindow.Show();
            videoFeedWindow.Activate();
        }

        private void TestLightOn_Click(object sender, RoutedEventArgs e)
        {
            GS_Comm.GetInstance().SendData(MemoryStructs.MemoryStruct_List.TEST_LIGHT, BitConverter.GetBytes(true));
        }

        private void TestLightOff_Click(object sender, RoutedEventArgs e)
        {
            GS_Comm.GetInstance().SendData(MemoryStructs.MemoryStruct_List.TEST_LIGHT, BitConverter.GetBytes(false));
        }

        private void SaveLayoutMenu_Click(object sender, RoutedEventArgs e)
        {
            var layoutSerializer = new XmlLayoutSerializer(mainDockManager);
            layoutSerializer.Serialize(@".\AvalonDock.Layout.config");
        }

        private void LoadLayoutMenu_Click(object sender, RoutedEventArgs e)
        {
            LoadLayout(false);
        }

        private void HID_InputMenu_Click(object sender, RoutedEventArgs e)
        {
            CreateDefaultLayout(HID_InputMenu.Header.ToString());
        }

        private void ConnectionSettingsMenu_Click(object sender, RoutedEventArgs e)
        {
            CreateDefaultLayout(ConnectionSettingsMenu.Header.ToString());            
        }

        private void ConnectionStatusMenu_Click(object sender, RoutedEventArgs e)
        {
            CreateDefaultLayout(ConnectionStatusMenu.Header.ToString());
        }

        private void MapMenu_Click(object sender, RoutedEventArgs e)
        {
            if (!WindowExists(MapMenu.Header.ToString()))
            {
                LayoutDocument MapLayout = new LayoutDocument();
                LayoutDocumentPane layoutPane = new LayoutDocumentPane(MapLayout);
                LayoutDocumentPaneGroup layoutGroup = new LayoutDocumentPaneGroup(layoutPane);
                MapLayout.Title = MapMenu.Header.ToString();                
                mainDockManager.Layout.RootPanel.Children.Add(layoutGroup);
                PopulateLayouts(false);
                double height = 768;
                double width = 1024;
                MapLayout.FloatingHeight = height;
                MapLayout.FloatingWidth = width;
                MapLayout.FloatingTop = DEFAULT_FLOAT_Y;
                MapLayout.FloatingLeft = DEFAULT_FLOAT_X;                                                
            }
        }

        private void GPS_StatusMenu_Click(object sender, RoutedEventArgs e)
        {
            CreateDefaultLayout(GPS_StatusMenu.Header.ToString());
        }

        private void HIL_Menu_Click(object sender, RoutedEventArgs e)
        {
            CreateDefaultLayout(HIL_Menu.Header.ToString());
        }

        private void AutopilotMenu_Click(object sender, RoutedEventArgs e)
        {
            CreateDefaultLayout(AutopilotMenu.Header.ToString());
        }

        private void ConsoleMenu_Click(object sender, RoutedEventArgs e)
        {
            CreateDefaultLayout(ConsoleMenu.Header.ToString());
        }

        #endregion        

        void connectionSettingsUC_OnAttemptConnect(object sender, ConnectionSettingsUC.ConnectArgs e)
        {
            connectionInfo_ = e;
            ConnectRaspberryPi();
        }

        void MainWindow_OnGpsOutputReceived(object sender, GS_Comm.GpsOutputArgs e)
        {
            if (MainMap_ != null)
            {
                Dispatcher.BeginInvoke(new Action(() => {
                    MainMap_.Position = new PointLatLng(e.gpsOutput_.latitude, e.gpsOutput_.longitude);
                    uavMarker_.Position = new PointLatLng(e.gpsOutput_.latitude, e.gpsOutput_.longitude);                    
                }));
            }
        }

        void MainWindow_OnYawPitchRollReceived(object sender, GS_Comm.YawPitchRollArgs e)
        {
            if (MainMap_ != null)
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {                   
                    uavImage_.RenderTransform = new RotateTransform(e.yaw_, uavImage_.Source.Width / 2, uavImage_.Source.Height / 2);
                }));
            }
        }

        void MainMap__MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (routeHelper_.runwaySetup)
            {
                float angle = (float)((Math.Atan2(e.GetPosition(MainMap_).Y - routeHelper_.rightClickPoint.Y, e.GetPosition(MainMap_).X - routeHelper_.rightClickPoint.X)));
                angle += (float)(Math.PI / 2.0f);

                if (angle < 0)
                    angle += (float)(2.0f * Math.PI);

                angle = angle * (float)(180.0f / Math.PI);

                routeHelper_.SetRunway(angle, MainMap_.FromLocalToLatLng((int)routeHelper_.rightClickPoint.X, (int)routeHelper_.rightClickPoint.Y));
            }
        }

        void MainMap__MouseMove(object sender, MouseEventArgs e)
        {
            if (routeHelper_.runwaySetup)
            {
                float angle = (float)((Math.Atan2(e.GetPosition(MainMap_).Y - routeHelper_.rightClickPoint.Y, e.GetPosition(MainMap_).X - routeHelper_.rightClickPoint.X)));
                angle += (float)(Math.PI / 2.0f);

                if (angle < 0)
                    angle += (float)(2.0f * Math.PI);

                angle = angle * (float)(180.0f / Math.PI);

                routeHelper_.SetRunwayHeading(angle);                
            }


        }

        void MainMap__MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            ContextMenu cm = new ContextMenu();

            MenuItem miSetRunway = new MenuItem();
            miSetRunway.Header = "Set Runway";
            cm.Items.Add(miSetRunway);
            miSetRunway.Click += miSetRunway_Click;

            MenuItem miAddWaypoint = new MenuItem();
            miAddWaypoint.Header = "Add Waypoint";
            cm.Items.Add(miAddWaypoint);
            miAddWaypoint.Click += miAddWaypoint_Click;

            cm.PlacementTarget = MainMap_;
            cm.IsOpen = true;
            routeHelper_.rightClickPoint = e.GetPosition(MainMap_);
        }

        void miAddWaypoint_Click(object sender, RoutedEventArgs e)
        {
            routeHelper_.AddWaypoint(MainMap_.FromLocalToLatLng((int)routeHelper_.rightClickPoint.X, (int)routeHelper_.rightClickPoint.Y));
        }

        void miSetRunway_Click(object sender, RoutedEventArgs e)
        {            
            routeHelper_.SetupRunway();
        }

        #endregion                           
    }


}
