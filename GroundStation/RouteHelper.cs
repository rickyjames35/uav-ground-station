﻿using GMap.NET;
using GMap.NET.WindowsPresentation;
using GroundStation.HID_Input;
using GroundStation.RouteComponents;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GroundStation
{
    public class RouteHelper
    {
        public enum RouteStatusEnum { UPLOADING, READY, NOT_READY, DOWNLOADING }

        public class Waypoint
        {
            public int waypointID;
            public MemoryStructs.WaypointTypeEnum waypointType;
            public GMapMarker waypoint;
            public int gotoIndex;
            public int iterations;
            public int IterationsLeft;
            public float altitude;
            public float heading;
            public float knots;
        }
        
        public class TransferPercentArgs : EventArgs
        {
            public float percent;

            public TransferPercentArgs(float percent)
            {
                this.percent = percent;
            }
        }

        public RouteStatusEnum RouteStatus
        {
            get { return routeStatus_; }
            set
            {
                routeStatus_ = value;
                if (OnRouteStatusChange != null)
                    OnRouteStatusChange(this, null);
            }
        }


        private const float DEFAULT_KNOTS = 100.0f;
        private const float DEFAULT_ALTITUDE = 1000.0f;
        private const int WAYPOINT_SEND_RATE = 100;

        public event EventHandler OnRouteUpdate;
        public event EventHandler OnRouteStatusChange;
        public event EventHandler<TransferPercentArgs> OnTransferPercentChange;

        public DataTable dtblWaypoints;
        public PointLatLng runwayCoordinates;
        public float runwayHeading;
        public Point rightClickPoint;
        public bool runwaySetup;
        public bool runwayPlaced;
        public Image runwayImage;
        public GMapMarker runwayMarker;
        public DataTable dtblRoute;
        public GMapControl map;
        public int currentWaypoint;
        public bool autopilotEnabled;

        Thread uploadThread;        
        BitmapImage runwayBmpSet;
        BitmapImage runwayBmpNotSet;

        public List<Waypoint> waypoints;
        GMapMarker wpRoute;
        GMapMarker uavMarker;
        RouteStatusEnum routeStatus_;

        public RouteHelper()
        {
            autopilotEnabled = false;
            runwaySetup = false;
            runwayPlaced = false;
            runwayImage = new Image();
            runwayMarker = new GMapMarker();
            waypoints = new List<Waypoint>();        

            runwayBmpSet = new BitmapImage();
            runwayBmpSet.BeginInit();
            runwayBmpSet.UriSource = new Uri("pack://application:,,,/GroundStation;component/Resources/runwaySet.png");
            runwayBmpSet.EndInit();

            runwayBmpNotSet = new BitmapImage();
            runwayBmpNotSet.BeginInit();
            runwayBmpNotSet.UriSource = new Uri("pack://application:,,,/GroundStation;component/Resources/runwayNotSet.png");
            runwayBmpNotSet.EndInit();

            runwayImage.Source = runwayBmpNotSet;
            runwayMarker.Shape = runwayImage;
            runwayMarker.Offset = new Point(-runwayImage.Source.Width / 2, -runwayImage.Source.Height);
            
            dtblRoute = new DataTable();
            dtblRoute.Columns.Add("Waypoint");
            dtblRoute.Columns.Add("Longitude", typeof(float));
            dtblRoute.Columns.Add("Latitude", typeof(float));
            dtblRoute.Columns.Add("Altitude", typeof(float));
            dtblRoute.Columns.Add("Heading", typeof(float));
            dtblRoute.Columns.Add("Knots", typeof(float));
            dtblRoute.Columns.Add("Iterations", typeof(int));
            dtblRoute.Columns.Add("IterationsLeft", typeof(int));

            dtblWaypoints = new DataTable();
            dtblWaypoints.Columns.Add("Name");
            dtblWaypoints.Columns.Add("Value", typeof(int));

            RouteStatus = RouteStatusEnum.NOT_READY;
            GS_Comm.GetInstance().OnWaypointUploadReady += RouteHelper_OnWaypointUploadReady;
            GS_Comm.GetInstance().OnWaypointUploadComplete += RouteHelper_OnWaypointUploadComplete;
            GS_Comm.GetInstance().OnCurrentWaypointReceived += RouteHelper_OnCurrentWaypointReceived;
            GS_Comm.GetInstance().OnAutopilotModeReceived += RouteHelper_OnAutopilotModeReceived;
            GS_Comm.GetInstance().OnWaypointReceived += RouteHelper_OnWaypointReceived;
            GS_Comm.GetInstance().OnWaypointDownloadComplete += RouteHelper_OnWaypointDownloadComplete;
            XBoxInput.GetInstance().OnAutopilotInputChanged += RouteHelper_OnAutopilotInputChanged;
        }

        void RouteHelper_OnWaypointDownloadComplete(object sender, EventArgs e)
        {
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                GenerateAll();
                RouteStatus = RouteStatusEnum.READY;
            });
        }        
        
        #region Methods

        #region Sets

        public void SetMap(ref GMapControl map)
        {
            this.map = map;
        }

        public void SetUAV(ref GMapMarker uavMarker)
        {
            this.uavMarker = uavMarker;
        }

        public void SetRunwayHeading(float heading)
        {
            runwayHeading = heading;
            runwayImage.RenderTransform = new RotateTransform(heading, runwayImage.Source.Width / 2, runwayImage.Source.Height);
        }

        public void SetRunwayPosition(PointLatLng coordinates)
        {
            runwayCoordinates = coordinates;
            runwayMarker.Position = coordinates;
        }

        public void SetupRunway()
        {
            SetRunwayPosition(map.FromLocalToLatLng((int)rightClickPoint.X, (int)rightClickPoint.Y));
            runwayImage.Source = runwayBmpNotSet;
            runwaySetup = true;
            runwayPlaced = false;
            GenerateAll();
        }

        public void SetRunway(float heading, PointLatLng coordinates)
        {
            SetRunwayPosition(coordinates);
            SetRunwayHeading(heading);
            runwayImage.Source = runwayBmpSet;
            runwaySetup = false;
            runwayPlaced = true;
            runwayHeading = heading;
            runwayCoordinates = coordinates;
            GenerateAll();
        }

        #endregion

        #region Adds

        public void AddTakeoff()
        {
            Waypoint wp = new Waypoint();
            wp.waypointType  = MemoryStructs.WaypointTypeEnum.TAKEOFF;            
            waypoints.Add(wp);
            GenerateAll();
        }

        public void AddLand()
        {
            Waypoint wp = new Waypoint();
            wp.waypointType = MemoryStructs.WaypointTypeEnum.LAND;
            waypoints.Add(wp);
            GenerateAll();
        }

        public void AddGoToWp(int wpID, int iterations)
        {
            Waypoint wp = new Waypoint();
            wp.waypointType = MemoryStructs.WaypointTypeEnum.GOTO;
            wp.iterations = iterations;
            wp.IterationsLeft = iterations;
            wp.gotoIndex = wpID;
            waypoints.Add(wp);
            GenerateAll();
        }

        public void AddWaypoint(PointLatLng coordinates)
        {
            int wpID = GetNewWayPointID();

            WaypointControl wc = new WaypointControl(wpID.ToString());
            Waypoint wp = new Waypoint();
            wp.waypointType = MemoryStructs.WaypointTypeEnum.WAYPOINT;
            wp.waypoint = new GMapMarker();
            wp.waypoint.Position = coordinates;
            wp.altitude = DEFAULT_ALTITUDE;
            wp.knots = DEFAULT_KNOTS;
            wp.waypoint.Shape = wc;
            wp.waypointID = wpID;
            waypoints.Add(wp);

            GenerateAll();
        }

        #endregion

        #region Edits

        public void ReorderWaypoint(int oldIndex, int newIndex)
        {
            if (newIndex < oldIndex)
            {
                Waypoint wp = waypoints[oldIndex];
                waypoints.RemoveAt(oldIndex);
                waypoints.Insert(newIndex, wp);
            }
            else if (newIndex > oldIndex)
            {
                Waypoint wp = waypoints[oldIndex];
                waypoints.Insert(newIndex, wp);
                waypoints.RemoveAt(oldIndex);
            }

            GenerateAll();
        }

        public void EditWaypoint(DataRow row)
        {
            int index = dtblRoute.Rows.IndexOf(row);
            if (waypoints[index].waypointType == MemoryStructs.WaypointTypeEnum.WAYPOINT)
            {
                waypoints[index].waypoint.Position = new PointLatLng(Convert.ToDouble(row["Latitude"]), Convert.ToDouble(row["Longitude"]));
                waypoints[index].altitude = (float)row["Altitude"];
            }
            else if (waypoints[index].waypointType == MemoryStructs.WaypointTypeEnum.GOTO)
            {
                waypoints[index].iterations = (int)row["Iterations"];
                waypoints[index].IterationsLeft = (int)row["Iterations"];
            }
            else if (waypoints[index].waypointType == MemoryStructs.WaypointTypeEnum.LAND || waypoints[index].waypointType == MemoryStructs.WaypointTypeEnum.TAKEOFF)
            {
                if (runwayPlaced)
                {
                    runwayCoordinates = new PointLatLng(Convert.ToDouble(row["Latitude"]), Convert.ToDouble(row["Longitude"]));
                    runwayHeading = (float)row["Heading"];
                    SetRunway(runwayHeading, runwayCoordinates);
                }
            }
            GenerateAll();
        }

        #endregion

        #region Gets

        private int GetWaypointIndex(int wpID)
        {
            for (int i = 0; i < waypoints.Count; i++)
            {
                if (waypoints[i].waypointID == wpID)
                    return i;
            }
            return -1;
        }

        int GetNewWayPointID()
        {
            int id = 0;
            bool idValid = false;
            while (!idValid)
            {
                idValid = true;
                foreach (Waypoint wp in waypoints)
                {
                    if (wp.waypointID == id)
                    {
                        idValid = false;
                        id++;
                        break;
                    }
                }
            }

            return id;
        }

        #endregion

        public void RemoveWaypoint(int index)
        {
            waypoints.RemoveAt(index);
            GenerateAll();
        }

        private void GenerateAll()
        {
            wpRoute = new GMapMarker(new PointLatLng());
            wpRoute.Shape = new Path();
            (wpRoute.Shape as Path).Stroke = Brushes.Red;
            (wpRoute.Shape as Path).StrokeThickness = 1;

            dtblRoute.Rows.Clear();
            dtblWaypoints.Rows.Clear();
            map.Markers.Clear();
            map.Markers.Add(uavMarker);
            map.Markers.Add(runwayMarker);
            map.Markers.Add(wpRoute);

            //build waypoints and datatables
            foreach (Waypoint wp in waypoints)
            {
                DataRow row = dtblRoute.NewRow();
                dtblRoute.Rows.Add(row);

                if (wp.waypointType == MemoryStructs.WaypointTypeEnum.WAYPOINT)
                {
                    DataRow row2 = dtblWaypoints.NewRow();
                    row2["Name"] = "Waypoint " + wp.waypointID;
                    row2["Value"] = wp.waypointID;
                    dtblWaypoints.Rows.Add(row2);

                    row["Waypoint"] = "Waypoint " + wp.waypointID;
                    row["Longitude"] = wp.waypoint.Position.Lng;
                    row["Latitude"] = wp.waypoint.Position.Lat;
                    row["Heading"] = 0;
                    row["Knots"] = wp.knots;
                    row["Altitude"] = wp.altitude;
                    row["Iterations"] = 0;
                    row["IterationsLeft"] = 0;
                    map.Markers.Add(wp.waypoint);
                }
                else if (wp.waypointType == MemoryStructs.WaypointTypeEnum.TAKEOFF)
                {
                    row["Waypoint"] = "Takeoff";
                    row["Longitude"] = runwayCoordinates.Lng;
                    row["Latitude"] = runwayCoordinates.Lat;
                    row["Heading"] = runwayHeading;
                    row["Knots"] = 0;
                    row["Altitude"] = 0;
                    row["Iterations"] = 0;
                    row["IterationsLeft"] = 0;
                }
                else if (wp.waypointType == MemoryStructs.WaypointTypeEnum.LAND)
                {
                    row["Waypoint"] = "Land";
                    row["Longitude"] = runwayCoordinates.Lng;
                    row["Latitude"] = runwayCoordinates.Lat;
                    row["Heading"] = runwayHeading;
                    row["Knots"] = 0;
                    row["Altitude"] = 0;
                    row["Iterations"] = 0;
                    row["IterationsLeft"] = 0;
                }
                else if (wp.waypointType == MemoryStructs.WaypointTypeEnum.GOTO)
                {
                    row["Waypoint"] = "Go To Waypoint " + wp.gotoIndex;
                    row["Longitude"] = 0;
                    row["Latitude"] = 0;
                    row["Heading"] = 0;
                    row["Knots"] = 0;
                    row["Altitude"] = 0;
                    row["Iterations"] = wp.iterations;
                    row["IterationsLeft"] = wp.IterationsLeft;
                }
            }

            //build route
            for (int i = 0; i < waypoints.Count; i++)
            {
                if (waypoints[i].waypointType == MemoryStructs.WaypointTypeEnum.GOTO)
                    waypoints[i].IterationsLeft = 1; //no reason to build it many times
            }

            for (int i = 0; i < waypoints.Count; i++)
            {
                if (waypoints[i].waypointType == MemoryStructs.WaypointTypeEnum.WAYPOINT)
                {
                    //route pos has to be the same as first wp in route
                    if (wpRoute.Position == new PointLatLng(0, 0))
                        wpRoute.Position = waypoints[i].waypoint.Position;

                    wpRoute.Route.Add(waypoints[i].waypoint.Position);
                }
                else if (waypoints[i].waypointType == MemoryStructs.WaypointTypeEnum.TAKEOFF ||
                          waypoints[i].waypointType == MemoryStructs.WaypointTypeEnum.LAND)
                {
                    //route pos has to be the same as first wp in route
                    if (wpRoute.Position == new PointLatLng(0, 0))
                        wpRoute.Position = runwayCoordinates;

                    wpRoute.Route.Add(runwayCoordinates);
                }
                else if (waypoints[i].waypointType == MemoryStructs.WaypointTypeEnum.GOTO)
                {
                    if (waypoints[i].IterationsLeft > 0)
                    {
                        waypoints[i].IterationsLeft--;
                        i = GetWaypointIndex(waypoints[i].gotoIndex) - 1;
                    }
                }
            }

            //reset iterations
            for (int i = 0; i < waypoints.Count; i++)
            {
                if (waypoints[i].waypointType == MemoryStructs.WaypointTypeEnum.GOTO)
                    waypoints[i].IterationsLeft = waypoints[i].iterations;
            }

            wpRoute.RegenerateRouteShape(map);


            if (OnRouteUpdate != null)
                OnRouteUpdate(this, null);
        }

        public void StartWaypointUpload()
        {
            RouteStatus = RouteStatusEnum.UPLOADING;
            MemoryStructs.StartWaypointUploadStruct swpu = new MemoryStructs.StartWaypointUploadStruct();
            swpu.waypointCount = (byte)waypoints.Count;
            GS_Comm.GetInstance().SendData(MemoryStructs.MemoryStruct_List.START_WAYPOINT_UPLOAD, MemoryStructs.StructToByteArray<MemoryStructs.StartWaypointUploadStruct>(swpu));
        }

        public void DownloadWaypoints()
        {
            waypoints = new List<Waypoint>();
            RouteStatus = RouteStatusEnum.DOWNLOADING;
            GS_Comm.GetInstance().SendData(MemoryStructs.MemoryStruct_List.WAYPOINT_DOWNLOAD_ROUTE, new byte[0]);
        }

        private void UploadWaypoints()
        {
            for (int i = 0; i < waypoints.Count; i++)
            {
                MemoryStructs.WaypointStruct wp = new MemoryStructs.WaypointStruct();
                wp.waypointIndex = (byte)i;
                wp.waypointID = (byte)waypoints[i].waypointID;
                wp.altitude = waypoints[i].altitude;
                wp.goToIndex = (byte)waypoints[i].gotoIndex;
                wp.iterations = (byte)waypoints[i].iterations;
                wp.waypointType = (byte)waypoints[i].waypointType;
                wp.knots = 0.0f;

                if (waypoints[i].waypointType == MemoryStructs.WaypointTypeEnum.WAYPOINT)
                {
                    wp.latitude = (float)waypoints[i].waypoint.Position.Lat;
                    wp.longitude = (float)waypoints[i].waypoint.Position.Lng;
                    wp.knots = waypoints[i].knots;
                }
                else if (waypoints[i].waypointType == MemoryStructs.WaypointTypeEnum.LAND || waypoints[i].waypointType == MemoryStructs.WaypointTypeEnum.TAKEOFF)
                {
                    wp.heading = runwayHeading;
                    wp.latitude = (float)runwayCoordinates.Lat;
                    wp.longitude = (float)runwayCoordinates.Lng;
                }
                GS_Comm.GetInstance().SendData(MemoryStructs.MemoryStruct_List.WAYPOINT, MemoryStructs.StructToByteArray<MemoryStructs.WaypointStruct>(wp));                                     
                
                if (OnTransferPercentChange != null)
                    OnTransferPercentChange(this, new TransferPercentArgs(((float)(i + 1) / (float)waypoints.Count)));

                Thread.Sleep(WAYPOINT_SEND_RATE); //don't overload the arduino with data
            }
            RouteStatus = RouteStatusEnum.READY;
        }

        private void SetCurrentWaypoint(int index)
        {
            MemoryStructs.CurrentWaypointStruct activeWp = new MemoryStructs.CurrentWaypointStruct();
            activeWp.waypointIndex = (byte)index;
            GS_Comm.GetInstance().SendData(MemoryStructs.MemoryStruct_List.SET_CURRENT_WAYPOINT, MemoryStructs.StructToByteArray<MemoryStructs.CurrentWaypointStruct>(activeWp));  
        }

        #endregion

        #region Events

        void RouteHelper_OnWaypointUploadReady(object sender, EventArgs e)
        {
            uploadThread = new Thread(UploadWaypoints);
            uploadThread.Start();            
        }

        void RouteHelper_OnWaypointUploadComplete(object sender, EventArgs e)
        {
            RouteStatus = RouteStatusEnum.READY;
            SetCurrentWaypoint(0);
        }

        void RouteHelper_OnCurrentWaypointReceived(object sender, GS_Comm.CurrentWaypointArgs e)
        {
            currentWaypoint = e.currentWaypoint_.waypointIndex;
            foreach (Waypoint wp in waypoints)
            {
                if (wp.waypointType == MemoryStructs.WaypointTypeEnum.WAYPOINT)
                {
                    if (currentWaypoint == wp.waypointID)
                        ((WaypointControl)wp.waypoint.Shape).SetActive();
                    else
                        ((WaypointControl)wp.waypoint.Shape).SetInactive();
                }
            }
        }

        void RouteHelper_OnAutopilotModeReceived(object sender, GS_Comm.AutopilotModeArgs e)
        {
            autopilotEnabled = e.autopilotMode_.autopilotEnabled;
        }

        void RouteHelper_OnAutopilotInputChanged(object sender, HID_Input.HID_Input.AutopilotInputArgs e)
        {
            MemoryStructs.AutoPilotModeStruct autopilotMode = new MemoryStructs.AutoPilotModeStruct();
            autopilotMode.autopilotEnabled = e.autopilotOn_;
            GS_Comm.GetInstance().SendData(MemoryStructs.MemoryStruct_List.REQUEST_AUTOPILOT_MODE, MemoryStructs.StructToByteArray<MemoryStructs.AutoPilotModeStruct>(autopilotMode));
        }

        void RouteHelper_OnWaypointReceived(object sender, GS_Comm.WaypointArgs e)
        {
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                Waypoint wp = new Waypoint();
                wp.altitude = e.waypoint_.altitude;
                wp.gotoIndex = e.waypoint_.goToIndex;
                wp.heading = e.waypoint_.heading;
                wp.iterations = e.waypoint_.iterations;
                wp.IterationsLeft = e.waypoint_.iterations;
                wp.knots = e.waypoint_.knots;
                wp.waypointID = e.waypoint_.waypointID;
                wp.waypointType = (MemoryStructs.WaypointTypeEnum)e.waypoint_.waypointType;

                if (wp.waypointType == MemoryStructs.WaypointTypeEnum.WAYPOINT)
                {
                    WaypointControl wc = new WaypointControl(wp.waypointID.ToString());
                    wp.waypoint = new GMapMarker();
                    wp.waypoint.Position = new PointLatLng(e.waypoint_.latitude, e.waypoint_.longitude);
                    wp.waypoint.Shape = wc;
                }

                waypoints.Insert(e.waypoint_.waypointIndex, wp);
            });            
        }

        #endregion

    }
}
