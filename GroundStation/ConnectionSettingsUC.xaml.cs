﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GroundStation
{
    /// <summary>
    /// Interaction logic for ConnectionSettingsUC.xaml
    /// </summary>
    public partial class ConnectionSettingsUC : UserControl
    {
        private DataSet dsConnectionSettings_;
        private bool autoConnect_;

        public class ConnectArgs : EventArgs
        {
            public string GroundStationIP;
            public string UAV_IP;

            public ConnectArgs(string GroundStationIP, string UAV_IP)
            {
                this.GroundStationIP = GroundStationIP;
                this.UAV_IP = UAV_IP;
            }
        }

        public event EventHandler<ConnectArgs> OnAttemptConnect;

        public ConnectionSettingsUC(bool autoConnect)
        {
            autoConnect_ = autoConnect;
            InitializeComponent();
        }

        private void btnSaveSettings_Click(object sender, RoutedEventArgs e)
        {
            dsConnectionSettings_.Tables[0].Rows[0]["GroundStationIP"] = cbAutoDetect.IsChecked.Value ? "auto" : txtGroundStationIP.Text;
            dsConnectionSettings_.Tables[0].Rows[0]["UAV_IP"] = txtUavIP.Text;
            dsConnectionSettings_.WriteXml("ConnectionSettings.xml");
            Connect();
        }

        private void cbAutoDetect_Checked(object sender, RoutedEventArgs e)
        {
            if (cbAutoDetect.IsChecked.Value)
            {
                txtGroundStationIP.Text = GetLocalIPAddress();
                txtGroundStationIP.IsReadOnly = true;
            }
            else
            {
                txtGroundStationIP.IsReadOnly = false;
            }
        }

        private string GetLocalIPAddress()
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                    break;
                }
            }
            return localIP;
        }

        public void Connect()
        {
            if (OnAttemptConnect != null)
                OnAttemptConnect(this, new ConnectArgs(dsConnectionSettings_.Tables[0].Rows[0]["GroundStationIP"].ToString() == "auto" ? GetLocalIPAddress() : dsConnectionSettings_.Tables[0].Rows[0]["GroundStationIP"].ToString(), dsConnectionSettings_.Tables[0].Rows[0]["UAV_IP"].ToString()));
        }

        private void LoadSettings()
        {
            dsConnectionSettings_.ReadXml("ConnectionSettings.xml");
            if (dsConnectionSettings_.Tables[0].Rows[0]["GroundStationIP"].ToString() == "auto")
                cbAutoDetect.IsChecked = true;
            else
            {
                cbAutoDetect.IsChecked = false;
                txtGroundStationIP.Text = dsConnectionSettings_.Tables[0].Rows[0]["GroundStationIP"].ToString();
            }

            txtUavIP.Text = dsConnectionSettings_.Tables[0].Rows[0]["UAV_IP"].ToString();
        }

        private void CreateDefaultSettings()
        {
            DataTable dtblConnectionSettings = new DataTable("ConnectionSettings");
            dtblConnectionSettings.Columns.Add("GroundStationIP");
            dtblConnectionSettings.Columns.Add("UAV_IP");
            DataRow mainRow = dtblConnectionSettings.NewRow();
            mainRow["GroundStationIP"] = "auto";
            mainRow["UAV_IP"] = "127.0.0.1";
            dtblConnectionSettings.Rows.Add(mainRow);
            dsConnectionSettings_.Tables.Add(dtblConnectionSettings);
            dsConnectionSettings_.WriteXml("ConnectionSettings.xml");
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            dsConnectionSettings_ = new DataSet();
            if (!File.Exists("ConnectionSettings.xml"))
                CreateDefaultSettings();
            LoadSettings();
            if (autoConnect_)
                Connect();
        }
    }
}
