﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GroundStation
{
    /// <summary>
    /// Interaction logic for ConnectionStatusUC.xaml
    /// </summary>
    public partial class ConnectionStatusUC : UserControl
    {
        private const int UPDATE_NOTIFY_LENGTH = 100;
        private const int PING_RATE = 1000;
        private const int PING_TIMEOUT = 3000;

        private Thread mainThread_;
        private bool running_;
        private DateTime arduinoHertzUpdate_;
        private bool arduinoHertzUpdated_;
        private DateTime raspberryPiHertzUpdate_;
        private bool raspberryPiHertzUpdated_;
        private Ping pingSender_;
        private bool pingReceived_;
        private DateTime lastPingSent_;
        private byte[] pingBuffer_;
        private PingOptions pingOptions_;

        public ConnectionStatusUC()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            running_ = true;
            mainThread_ = new Thread(Update);
            mainThread_.Start();
            GS_Comm.GetInstance().OnArduinoHertzRateReceived += ConnectionStatusUC_OnArduinoHertzRateReceived;
            GS_Comm.GetInstance().OnRaspberryPiHertzRateReceived += ConnectionStatusUC_OnRaspberryPiHertzRateReceived;
            GS_Comm.GetInstance().OnArduinoConnect += ConnectionStatusUC_OnArduinoConnect;
            GS_Comm.GetInstance().OnArduinoDisconnect += ConnectionStatusUC_OnArduinoDisconnect;
            GS_Comm.GetInstance().OnRaspberryPiConnect += ConnectionStatusUC_OnRaspberryPiConnect;
            GS_Comm.GetInstance().OnRaspberryPiDisconnect += ConnectionStatusUC_OnRaspberryPiDisconnect;
            lastPingSent_ = DateTime.Now;
            pingReceived_ = true;
            arduinoHertzUpdate_ = DateTime.Now;
            arduinoHertzUpdated_ = false;
            raspberryPiHertzUpdate_ = DateTime.Now;
            raspberryPiHertzUpdated_ = false;
            pingBuffer_ = new byte[32];
            pingOptions_ = new PingOptions(64, true);
            pingSender_ = new Ping();
            pingSender_.PingCompleted += pingSender__PingCompleted;
            SetArduinoStatus(GS_Comm.GetInstance().IsArduinoConnected());
            SetRaspberryPiStatus(GS_Comm.GetInstance().IsRaspberryPiConnected());
        }

        void ConnectionStatusUC_OnRaspberryPiDisconnect(object sender, EventArgs e)
        {
            SetRaspberryPiStatus(false);
        }

        void ConnectionStatusUC_OnRaspberryPiConnect(object sender, EventArgs e)
        {
            SetRaspberryPiStatus(true);
        }

        void ConnectionStatusUC_OnArduinoDisconnect(object sender, EventArgs e)
        {
            SetArduinoStatus(false);
        }

        void ConnectionStatusUC_OnArduinoConnect(object sender, EventArgs e)
        {
            SetArduinoStatus(true);
        }

        void SetRaspberryPiStatus(bool connected)
        {
            Dispatcher.BeginInvoke(new Action(() => { lblRaspberryPiStatus.Content = "Raspberry Pi Status: " + (connected ? "Connected" : "Disconnected"); }));
        }

        void SetArduinoStatus(bool connected)
        {
            Dispatcher.BeginInvoke(new Action(() => { lblArduinoStatus.Content = "Arduino Status: " + (connected ? "Connected" : "Disconnected"); }));
        }

        void pingSender__PingCompleted(object sender, PingCompletedEventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() => { lblUavLatency.Content = "UAV Latency: " + (e.Reply.Status == IPStatus.Success ? e.Reply.RoundtripTime + "ms" : ((Enum)e.Reply.Status).ToString()); }));
            pingReceived_ = true;  
        }

        void ConnectionStatusUC_OnRaspberryPiHertzRateReceived(object sender, GS_Comm.RaspberryPiHertzRateArgs e)
        {
            raspberryPiHertzUpdate_ = DateTime.Now;
            raspberryPiHertzUpdated_ = true;
            Dispatcher.BeginInvoke(new Action(() =>
            {
                lblRaspberryPiHz.Content = "Raspberry Pi Hertz: " + e.rasPiUpdateHz_.updateRate;
                pbSignalQuality.Value = e.rasPiUpdateHz_.signalStrength * 100.0F;
                lblArduinoRx.Content = "Arduino Rx: " + e.rasPiUpdateHz_.rxBps + " bps";
                lblArduinoTx.Content = "Arduino Tx: " + e.rasPiUpdateHz_.txBps + " bps";
            }));   
        }

        void ConnectionStatusUC_OnArduinoHertzRateReceived(object sender, GS_Comm.ArduinoHertzRateArgs e)
        {
            arduinoHertzUpdate_ = DateTime.Now;
            arduinoHertzUpdated_ = true;
            Dispatcher.BeginInvoke(new Action(() => { 
                lblArduinoHertz.Content = "Arduino Hertz: " + e.arduinoHertz_.updateRate;
                lblArduinoMemory.Content = "Arduino Free Memory: " + e.arduinoHertz_.availableMemory + " bytes";
            }));
        }

        private void Update()
        {
            while (running_)
            {
                if (arduinoHertzUpdated_ && (DateTime.Now - arduinoHertzUpdate_).TotalMilliseconds <= UPDATE_NOTIFY_LENGTH)
                {
                    Dispatcher.BeginInvoke(new Action(() => { arduinoUpdateEllipse.Fill = System.Windows.Media.Brushes.Green; }));
                }
                else
                {
                    arduinoHertzUpdated_ = false;
                    Dispatcher.BeginInvoke(new Action(() => { arduinoUpdateEllipse.Fill = System.Windows.Media.Brushes.White; }));
                }

                if (raspberryPiHertzUpdated_ && (DateTime.Now - raspberryPiHertzUpdate_).TotalMilliseconds <= UPDATE_NOTIFY_LENGTH)
                {
                    Dispatcher.BeginInvoke(new Action(() => { raspberryPiUpdateEllipse.Fill = System.Windows.Media.Brushes.Green; }));
                }
                else
                {
                    raspberryPiHertzUpdated_ = false;
                    Dispatcher.BeginInvoke(new Action(() => { raspberryPiUpdateEllipse.Fill = System.Windows.Media.Brushes.White; }));
                }

                if (pingReceived_ && (DateTime.Now - lastPingSent_).TotalMilliseconds >= PING_RATE)
                {
                    lastPingSent_ = DateTime.Now;
                    if (GS_Comm.GetInstance().IsRunning())
                    {
                        pingReceived_ = false;
                        //Dispatcher.BeginInvoke(new Action(() => { pingSender_.SendAsync(GS_Comm.GetInstance().GetRemoteIP(), PING_TIMEOUT, pingBuffer_, pingOptions_, null); }));
                    }
                }
                Thread.Sleep(50);
            }
            pingSender_.SendAsyncCancel();
            pingSender_.Dispose();
        }

        private void UserControl_Unloaded_1(object sender, RoutedEventArgs e)
        {
            running_ = false;
            GS_Comm.GetInstance().OnArduinoHertzRateReceived -= ConnectionStatusUC_OnArduinoHertzRateReceived;
            GS_Comm.GetInstance().OnRaspberryPiHertzRateReceived -= ConnectionStatusUC_OnRaspberryPiHertzRateReceived;
            mainThread_.Join(100);
        }
    }
}
