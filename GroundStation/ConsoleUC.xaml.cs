﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GroundStation
{
    /// <summary>
    /// Interaction logic for ConsoleUC.xaml
    /// </summary>
    public partial class ConsoleUC : UserControl
    {
        public ConsoleUC()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            GS_Comm.GetInstance().OnConsoleOutputReceived += ConsoleUC_OnConsoleOutputReceived;
        }

        void ConsoleUC_OnConsoleOutputReceived(object sender, GS_Comm.ConsoleArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                txtConsole.Text += (DateTime.Now.ToLongTimeString() + ": " + new string(e.consoleOutput_.console) + Environment.NewLine);
                txtConsole.ScrollToEnd();
            }));
        }
    }
}
