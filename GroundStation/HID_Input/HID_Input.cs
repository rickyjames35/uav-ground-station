﻿using SlimDX;
using SlimDX.DirectInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Interop;

namespace GroundStation.HID_Input
{
    abstract class HID_Input
    {
        public class AileronElevatorInputArgs : EventArgs
        {
            public float aileron_;
            public float elevator_;

            public AileronElevatorInputArgs(float aileron, float elevator)
            {
                aileron_ = aileron;
                elevator_ = elevator;
            }
        }

        public class RudderInputArgs : EventArgs
        {
            public float rudder_;            

            public RudderInputArgs(float rudder)
            {
                rudder_ = rudder;
            }
        }

        protected class StateReceivedArgs : EventArgs
        {
            public JoystickState state_;

            public StateReceivedArgs(JoystickState state)
            {
                state_ = state;
            }
        }

        public class JoystickConnectionChangedArgs : EventArgs
        {
            public bool connected_;

            public JoystickConnectionChangedArgs(bool connected)
            {
                connected_ = connected;
            }
        }

        public class ThrottleInputArgs : EventArgs
        {
            public float throttle_;

            public ThrottleInputArgs(float throttle)
            {
                throttle_ = throttle;
            }
        }

        public class EngineStatusInputArgs : EventArgs
        {
            public bool engineOn_;

            public EngineStatusInputArgs(bool engineOn)
            {
                engineOn_ = engineOn;
            }
        }

        public class FlapsStatusInputArgs : EventArgs
        {
            public bool flapsDown_;

            public FlapsStatusInputArgs(bool flapsDown)
            {
                flapsDown_ = flapsDown;
            }
        }

        public class AutopilotInputArgs : EventArgs
        {
            public bool autopilotOn_;

            public AutopilotInputArgs(bool autopilotOn)
            {
                autopilotOn_ = autopilotOn;
            }
        }

        public bool JoystickConnected
        {
            get {  return joystick_ != null; }
        }

        public float Ailerons
        {
            get { return aileron_; }
        }

        public float Elevators
        {
            get { return elevator_; }
        }

        public float Rudders
        {
            get { return rudder_; }
        }

        public float Throttle
        {
            get { return throttle_; }
        }

        public bool FlapsDown
        {
            get { return flapsDown_; }
        }

        public bool EngineOn
        {
            get { return engineOn_; }
        }

        public bool AutopilotOn
        {
            get { return autopilotOn_; }
        }

        public event EventHandler<AileronElevatorInputArgs> OnAileronElevatorInputChanged;
        public event EventHandler<RudderInputArgs> OnRudderInputChanged;
        public event EventHandler<JoystickConnectionChangedArgs> OnJoystickConnectionChanged;
        public event EventHandler<ThrottleInputArgs> OnThrottleInputChanged;
        public event EventHandler<EngineStatusInputArgs> OnEngineStatusInputChanged;
        public event EventHandler<FlapsStatusInputArgs> OnFlapsStatusInputChanged;
        public event EventHandler<AutopilotInputArgs> OnAutopilotInputChanged;

        protected event EventHandler<StateReceivedArgs> OnStateReceived;

        protected JoystickState state_;
        protected float aileron_;
        protected float elevator_;
        protected float rudder_;
        protected float throttle_;
        protected bool flapsDown_;
        protected bool engineOn_;
        protected bool autopilotOn_;

        Joystick joystick_;
        Thread mainThread_;
        IntPtr handle_;
        bool running_;
        DirectInput dinput;

        public HID_Input()
        {
            running_ = false;
            state_ = new JoystickState();
            ResetValues();
        }

        protected void ResetValues()
        {            
            aileron_ = elevator_ = rudder_ = throttle_ = 0;
            flapsDown_ = false;
            engineOn_ = false;
            autopilotOn_ = false;
        }

        protected void CallOnAileronElevatorInputChanged(object sender, AileronElevatorInputArgs args)
        {
            if (OnAileronElevatorInputChanged != null)
                OnAileronElevatorInputChanged(sender, args);
        }

        protected void CallOnRudderInputChanged(object sender, RudderInputArgs args)
        {
            if (OnRudderInputChanged != null)
                OnRudderInputChanged(sender, args);
        }

        protected void CallOnThrottleInputChanged(object sender, ThrottleInputArgs args)
        {
            if (OnThrottleInputChanged != null)
                OnThrottleInputChanged(sender, args);
        }

        protected void CallOnEngineStatusInputChanged(object sender, EngineStatusInputArgs args)
        {
            if (OnEngineStatusInputChanged != null)
                OnEngineStatusInputChanged(sender, args);
        }

        protected void CallOnFlapsStatusInputChanged(object sender, FlapsStatusInputArgs args)
        {
            if (OnFlapsStatusInputChanged != null)
                OnFlapsStatusInputChanged(sender, args);
        }

        protected void CallOnAutopilotInputChanged(object sender, AutopilotInputArgs args)
        {
            if (OnAutopilotInputChanged != null)
                OnAutopilotInputChanged(sender, args);
        }

        public void Start(IntPtr handle)
        {
            handle_ = handle;
            running_ = true;
            mainThread_ = new Thread(Update);
            mainThread_.Start();
        }

        public void Stop()
        {
            running_ = false;
            mainThread_.Join(500);
            ReleaseJoystick();
            dinput.Dispose();
        }

        private void ReleaseJoystick()
        {
            if (joystick_ != null)
            {
                joystick_.Unacquire();
                joystick_.Dispose();
            }
            joystick_ = null;

            if (OnJoystickConnectionChanged != null)
                OnJoystickConnectionChanged(this, new JoystickConnectionChangedArgs(JoystickConnected));
        }

        private void Update()
        {
            while (running_)
            {
                if (joystick_ == null)
                {
                    CreateDevice();
                    Thread.Sleep(1000);
                }
                else
                {
                    try
                    {
                        if (!joystick_.Acquire().IsFailure && !joystick_.Poll().IsFailure)
                        {
                            JoystickState newState = joystick_.GetCurrentState();

                            if (Result.Last.IsFailure)
                                ReleaseJoystick();

                            if (OnStateReceived != null)
                                OnStateReceived(this, new StateReceivedArgs(newState));
                        }
                        else
                        {
                            ReleaseJoystick();
                        }
                    }
                    catch
                    {
                        ReleaseJoystick();
                    }

                    Thread.Sleep(5);
                }
            }
        }

        private void CreateDevice()
        {
            // make sure that DirectInput has been initialized
            dinput = new DirectInput();
            
            // search for devices
            foreach (DeviceInstance device in dinput.GetDevices(DeviceClass.GameController, DeviceEnumerationFlags.AttachedOnly))
            {
                // create the device
                try
                {
                    joystick_ = new Joystick(dinput, device.InstanceGuid);
                    joystick_.SetCooperativeLevel(handle_, CooperativeLevel.Nonexclusive | CooperativeLevel.Background);                    
                    break;
                }
                catch (DirectInputException)
                {

                }
            }

            if (joystick_ == null)
                return;

            // acquire the device
            joystick_.Acquire();

            if (OnJoystickConnectionChanged != null)
                OnJoystickConnectionChanged(this, new JoystickConnectionChangedArgs(JoystickConnected));
        }
    }
}
