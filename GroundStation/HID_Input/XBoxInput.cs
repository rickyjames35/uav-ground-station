﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroundStation.HID_Input
{
    class XBoxInput : HID_Input
    {
        private const float stickDeadZone = 0.13F;
        private const float changeThreshold = 0.001F;
        private const int maxStickValue = 65535;
        private const int flapsChangeHoldDownTime = 500;
        private const int engineChangeHoldDownTime = 1000;        
        private const int startButtonIndex = 7;
        private const int backButtonIndex = 6;
        private const int yButtonIndex = 3;
        private const int aButtonIndex = 0;
        private const int leftBumperIndex = 4;
        private const int rightBumberIndex = 5;

        private static XBoxInput instance_;

        DateTime flapsDownTime;
        DateTime flapsUpTime;
        DateTime engineOnTime;
        DateTime engineOffTime;

        public XBoxInput()
        {
            flapsDownTime = flapsUpTime = engineOnTime = engineOffTime = DateTime.MaxValue;
            OnStateReceived += XBoxInput_OnStateReceived;
        }

        public static XBoxInput GetInstance()
        {
            if (instance_ == null)
                instance_ = new XBoxInput();

            return instance_;
        }

        void XBoxInput_OnStateReceived(object sender, HID_Input.StateReceivedArgs e)
        {
            //Autopilot
            if (e.state_.GetButtons()[leftBumperIndex])
            {
                if (autopilotOn_ != false)
                {
                    autopilotOn_ = false;
                    CallOnAutopilotInputChanged(this, new AutopilotInputArgs(autopilotOn_));
                }
            }
            else if (e.state_.GetButtons()[rightBumberIndex])
            {
                if (autopilotOn_ != true)
                {
                    autopilotOn_ = true;
                    CallOnAutopilotInputChanged(this, new AutopilotInputArgs(autopilotOn_));
                }
            }

            //Aileron & Elevator
            if (PastThreshold(NormalizeStickState(state_.X), NormalizeStickState(e.state_.X)) || PastThreshold(NormalizeStickState(state_.Y), NormalizeStickState(e.state_.Y)))
            {
                aileron_ = ApplyDeadband(NormalizeStickState(e.state_.X), 0.02f);
                elevator_ = ApplyDeadband(NormalizeStickState(e.state_.Y), 0.02f);
                CallOnAileronElevatorInputChanged(this, new AileronElevatorInputArgs(aileron_, elevator_));
            }

            //Rudder
            if (PastThreshold(NormalizeStickState(state_.RotationX), NormalizeStickState(e.state_.RotationX)))
            {
                rudder_ = ApplyDeadband(-NormalizeStickState(e.state_.RotationX), 0.02f);
                CallOnRudderInputChanged(this, new RudderInputArgs(rudder_));
            }

            //Throttle
            if (PastThreshold(NormalizeThrottleState(state_.Z), NormalizeThrottleState(e.state_.Z)))
            {
                throttle_ = ApplyDeadband(NormalizeThrottleState(e.state_.Z), 0.02f);
                CallOnThrottleInputChanged(this, new ThrottleInputArgs(throttle_));
            }

            //Engine On
            if (e.state_.GetButtons()[startButtonIndex])
            {
                if (engineOnTime == DateTime.MaxValue)
                {
                    engineOnTime = DateTime.Now;
                }
                else if (engineOnTime != DateTime.MinValue)
                {
                    if ((DateTime.Now - engineOnTime).TotalMilliseconds >= engineChangeHoldDownTime)
                    {
                        engineOnTime = DateTime.MinValue;
                        engineOn_ = true;
                        CallOnEngineStatusInputChanged(this, new EngineStatusInputArgs(engineOn_));
                    }
                }
            }
            else
            {
                engineOnTime = DateTime.MaxValue;
            }

            //Engine Off
            if (e.state_.GetButtons()[backButtonIndex])
            {
                if (engineOffTime == DateTime.MaxValue)
                {
                    engineOffTime = DateTime.Now;
                }
                else if (engineOffTime != DateTime.MinValue)
                {
                    if ((DateTime.Now - engineOffTime).TotalMilliseconds >= engineChangeHoldDownTime)
                    {
                        engineOffTime = DateTime.MinValue;
                        engineOn_ = false;
                        CallOnEngineStatusInputChanged(this, new EngineStatusInputArgs(engineOn_));
                    }
                }
            }
            else
            {
                engineOffTime = DateTime.MaxValue;
            }

            //Flaps Up
            if (e.state_.GetButtons()[yButtonIndex])
            {
                if (flapsUpTime == DateTime.MaxValue)
                {
                    flapsUpTime = DateTime.Now;
                }
                else if (flapsUpTime != DateTime.MinValue)
                {
                    if ((DateTime.Now - flapsUpTime).TotalMilliseconds >= flapsChangeHoldDownTime)
                    {                        
                        flapsUpTime = DateTime.MinValue;
                        flapsDown_ = false;
                        CallOnFlapsStatusInputChanged(this, new FlapsStatusInputArgs(flapsDown_));
                    }
                }
            }
            else
            {
                flapsUpTime = DateTime.MaxValue;
            }

            //Flaps Down
            if (e.state_.GetButtons()[aButtonIndex])
            {
                if (flapsDownTime == DateTime.MaxValue)
                {
                    flapsDownTime = DateTime.Now;
                }
                else if (flapsDownTime != DateTime.MinValue)
                {
                    if ((DateTime.Now - flapsDownTime).TotalMilliseconds >= flapsChangeHoldDownTime)
                    {
                        flapsDownTime = DateTime.MinValue;
                        flapsDown_ = true;
                        CallOnFlapsStatusInputChanged(this, new FlapsStatusInputArgs(flapsDown_));
                    }
                }
            }
            else
            {
                flapsDownTime = DateTime.MaxValue;
            }

            state_ = e.state_;
        }

        float ApplyDeadband(float value, float deadband)
        {
            if (Math.Abs(value) <= deadband)
            {
                return 0.0f;
            }
            else
            {
                if (value >= 0.0)
                    return (value - deadband) * (1.0f / (1.0f - deadband));
                else
                    return (value + deadband) * (1.0f / (1.0f - deadband));
            }
        }

        float NormalizeThrottleState(int input)
        {
            float realValue = ((1.0F - ((float)input / (float)maxStickValue)) - 0.5F) * 2.0F;
            return realValue >= 0.0F ? realValue : 0.0F;
        }

        float NormalizeStickState(int input)
        {
            float realValue = (((float)input / (float)maxStickValue) * 2.0F) - 1.0F;

            if (Math.Abs(realValue) >= stickDeadZone)
            {
                if (realValue > 0)
                    return (realValue - stickDeadZone) * (1 / (1 - stickDeadZone));
                else
                    return (realValue + stickDeadZone) * (1 / (1 - stickDeadZone));
            }
            else
                return 0.0F;
        }

        bool PastThreshold(float lastState, float currentState)
        {
            if (Math.Abs(lastState - currentState) > changeThreshold)
                return true;
            else
                return false;
        }
    }
}
