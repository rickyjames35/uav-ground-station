﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GroundStation
{
    /// <summary>
    /// Interaction logic for AutopilotUC.xaml
    /// </summary>
    public partial class AutopilotUC : UserControl
    {
        private RouteHelper routeHelper_;
        private DataGridRow draggingRow_ = null;
        private double percentTransfered;

        public AutopilotUC()
        {
            InitializeComponent();
            GS_Comm.GetInstance().OnArduinoConnect += AutopilotUC_OnArduinoConnectionChange;
            GS_Comm.GetInstance().OnArduinoDisconnect += AutopilotUC_OnArduinoConnectionChange;
            GS_Comm.GetInstance().OnAutopilotModeReceived += AutopilotUC_OnAutopilotModeReceived;
            percentTransfered = 0;
        }        

        void AutopilotUC_OnAutopilotModeReceived(object sender, GS_Comm.AutopilotModeArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                lblAutopilotEnabled.Content = "Autopilot Enabled: " + (e.autopilotMode_.autopilotEnabled ? "Yes" : "No");
            }));
        }

        #region Methods

        public void SetRouteInfo(ref RouteHelper routeHelper)
        {
            routeHelper_ = routeHelper;
            dataGridRoute.ItemsSource = routeHelper_.dtblRoute.AsDataView();
            cmbWaypoints.ItemsSource = routeHelper_.dtblWaypoints.AsDataView();
            cmbWaypoints.DisplayMemberPath = "Name";
            cmbWaypoints.SelectedValuePath = "Value";
            routeHelper_.OnRouteUpdate += routeHelper__OnRouteUpdate;
            routeHelper_.OnRouteStatusChange += routeHelper__OnRouteStatusChange;
            routeHelper_.OnTransferPercentChange += routeHelper__OnTransferPercentChange;
            routeHelper__OnRouteUpdate(this, null);
            RefreshControls();
        }

        #endregion

        #region Events

        void AutopilotUC_OnArduinoConnectionChange(object sender, EventArgs e)
        {
            RefreshControls();
        }

        void routeHelper__OnRouteUpdate(object sender, EventArgs e)
        {
            txtRunwayHeading.Text = routeHelper_.runwayHeading.ToString();
            txtRunwayLat.Text = ((float)routeHelper_.runwayCoordinates.Lat).ToString();
            txtRunwayLon.Text = ((float)routeHelper_.runwayCoordinates.Lng).ToString();
            if (routeHelper_.dtblWaypoints.Rows.Count > 0)
                cmbWaypoints.SelectedIndex = 0;
        }

        private void btnTakeoff_Click(object sender, RoutedEventArgs e)
        {
            routeHelper_.AddTakeoff();
        }

        private void btnLand_Click(object sender, RoutedEventArgs e)
        {
            routeHelper_.AddLand();
        }

        private void btnAddGoto_Click(object sender, RoutedEventArgs e)
        {
            routeHelper_.AddGoToWp((int)cmbWaypoints.SelectedValue, Convert.ToInt32(txtIterations.Text));
        }

        private void dataGridRoute_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!dataGridRoute.IsReadOnly)
            {
                draggingRow_ = DraggingHelper.TryFindFromPoint<DataGridRow>((UIElement)sender, e.GetPosition(dataGridRoute));
                if (draggingRow_ != null && !draggingRow_.IsEditing)
                    DragDrop.DoDragDrop(dataGridRoute, dataGridRoute.Items[draggingRow_.GetIndex()], DragDropEffects.Move);
            }
        }

        private void dataGridRoute_Drop(object sender, DragEventArgs e)
        {
            DataGridRow newRow = DraggingHelper.TryFindFromPoint<DataGridRow>((UIElement)sender, e.GetPosition(dataGridRoute));

            if (newRow != null)
            {
                if (draggingRow_ == newRow)
                    draggingRow_.IsSelected = true;
                else
                    routeHelper_.ReorderWaypoint(draggingRow_.GetIndex(), newRow.GetIndex());

                draggingRow_ = null;
            }
        }

        private void RowEditEnded(DataRowView row)
        {
            routeHelper_.EditWaypoint(row.Row);
        }

        private void dataGridRoute_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Commit)
                Dispatcher.BeginInvoke(new Action(() => RowEditEnded((DataRowView)e.Row.Item)), System.Windows.Threading.DispatcherPriority.Background);
        }

        private void dataGridRoute_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete && !dataGridRoute.IsReadOnly)
            {
                if (dataGridRoute.SelectedIndex != -1)
                    routeHelper_.RemoveWaypoint(dataGridRoute.SelectedIndex);
            }
        }

        private void txtRunway_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (routeHelper_ != null && routeHelper_.runwayPlaced)
                routeHelper_.SetRunway((float)Convert.ToDouble(txtRunwayHeading.Text), new GMap.NET.PointLatLng(Convert.ToDouble(txtRunwayLat.Text), Convert.ToDouble(txtRunwayLon.Text)));
        }

        private void btnActivate_Click(object sender, RoutedEventArgs e)
        {
            if (routeHelper_.RouteStatus == RouteHelper.RouteStatusEnum.NOT_READY)
            {
                routeHelper_.StartWaypointUpload();
            }
            else if (routeHelper_.RouteStatus == RouteHelper.RouteStatusEnum.READY)
            {                
                routeHelper_.RouteStatus = RouteHelper.RouteStatusEnum.NOT_READY;
            }
        }

        void routeHelper__OnRouteStatusChange(object sender, EventArgs e)
        {
            RefreshControls();
        }

        void routeHelper__OnTransferPercentChange(object sender, RouteHelper.TransferPercentArgs e)
        {
            percentTransfered = e.percent;
            RefreshControls();              
        }

        #endregion

        private void dataGridRoute_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            RefreshControls();          
        }

        private void btnSetActiveWaypoint_Click(object sender, RoutedEventArgs e)
        {

            //stopped here
        }

        private void btnSetIterationsLeft_Click(object sender, RoutedEventArgs e)
        {

        }

        void RefreshControls()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                string btnActivateContent = "";
                string lblStatusContent = "";
                string txtIterationsLeftText = "";
                bool btnSetActiveWaypointEnabled = false;
                bool btnActivateIsEnabled = false;
                double progressBarTransferValue = 0;

                if (GS_Comm.GetInstance().IsArduinoConnected())
                {
                    if (routeHelper_.RouteStatus == RouteHelper.RouteStatusEnum.UPLOADING)
                    {
                        btnActivateContent = "Uploading";
                        lblStatusContent = "Uploading...";
                        btnSetActiveWaypointEnabled = false;
                        btnActivateIsEnabled = false;
                        progressBarTransferValue = percentTransfered;
                        dataGridRoute.IsReadOnly = true;
                    }
                    else if (routeHelper_.RouteStatus == RouteHelper.RouteStatusEnum.READY)
                    {
                        btnActivateContent = "Edit current route";
                        lblStatusContent = "";

                        int selectedIndex = dataGridRoute.SelectedIndex;
                        if (selectedIndex >= 0)
                        {
                            if (routeHelper_.waypoints[selectedIndex].waypointType == MemoryStructs.WaypointTypeEnum.GOTO)
                            {
                                txtIterationsLeftText = routeHelper_.waypoints[selectedIndex].IterationsLeft.ToString();
                            }
                            btnSetActiveWaypointEnabled = false;
                            dataGridRoute.IsReadOnly = true;
                        }
                                                                                
                        btnActivateIsEnabled = true;
                        progressBarTransferValue = 0;
                    }
                    else if (routeHelper_.RouteStatus == RouteHelper.RouteStatusEnum.NOT_READY)
                    {
                        btnActivateContent = "Upload route";
                        lblStatusContent = "";
                        btnSetActiveWaypointEnabled = false;
                        btnActivateIsEnabled = true;
                        progressBarTransferValue = 0;
                        dataGridRoute.IsReadOnly = false;
                    }
                    else if (routeHelper_.RouteStatus == RouteHelper.RouteStatusEnum.DOWNLOADING)
                    {
                        btnActivateContent = "Upload route";
                        lblStatusContent = "Downloading...";
                        btnSetActiveWaypointEnabled = false;
                        btnActivateIsEnabled = true;
                        progressBarTransferValue = 0;
                        dataGridRoute.IsReadOnly = true;
                    }
                }
                else
                {
                    btnActivateContent = "Disconnected";
                    lblStatusContent = "";
                    btnSetActiveWaypointEnabled = false;                
                    btnActivateIsEnabled = false;
                    progressBarTransferValue = 0;
                }


                btnActivate.Content = btnActivateContent;
                lblStatus.Content = lblStatusContent;
                btnSetActiveWaypoint.IsEnabled = btnSetActiveWaypointEnabled;
                btnActivate.IsEnabled = btnActivateIsEnabled;
                progressBarTransfer.Value = progressBarTransferValue;
            }));
        }

        private void btnDownloadRoute_Click(object sender, RoutedEventArgs e)
        {
            routeHelper_.DownloadWaypoints();
        }
    }

    public static class DraggingHelper
    {
        public static T TryFindFromPoint<T>(UIElement reference, Point point) where T : DependencyObject
        {
            DependencyObject element = reference.InputHitTest(point) as DependencyObject;
            if (element == null) return null;
            else if (element is T) return (T)element;
            else return TryFindParent<T>(element);
        }

        public static T TryFindParent<T>(this DependencyObject child) where T : DependencyObject
        {
            //get parent item
            DependencyObject parentObject = GetParentObject(child);

            //we've reached the end of the tree
            if (parentObject == null) return null;

            //check if the parent matches the type we're looking for
            T parent = parentObject as T;
            if (parent != null)
            {
                return parent;
            }
            else
            {
                //use recursion to proceed with next level
                return TryFindParent<T>(parentObject);
            }
        }

        public static DependencyObject GetParentObject(this DependencyObject child)
        {
            if (child == null) return null;

            //handle content elements separately
            ContentElement contentElement = child as ContentElement;
            if (contentElement != null)
            {
                DependencyObject parent = ContentOperations.GetParent(contentElement);
                if (parent != null) return parent;

                FrameworkContentElement fce = contentElement as FrameworkContentElement;
                return fce != null ? fce.Parent : null;
            }

            //also try searching for parent in framework elements (such as DockPanel, etc)
            FrameworkElement frameworkElement = child as FrameworkElement;
            if (frameworkElement != null)
            {
                DependencyObject parent = frameworkElement.Parent;
                if (parent != null) return parent;
            }

            //if it's not a ContentElement/FrameworkElement, rely on VisualTreeHelper
            return VisualTreeHelper.GetParent(child);
        }
    }
}
