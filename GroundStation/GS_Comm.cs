﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace GroundStation
{
    class GS_Comm
    {
        private const int GS_COMM_UPDATE_HERTZ = 1;
        private const int LISTEN_PORT = 5001;
        private const int SEND_PORT = 5000;
        private const int NETWORK_BUFFER_SIZE = 5120;
        private const int TEMP_NETWORK_BUFFER_SIZE = 1024;
        private const int RASPBERRY_PI_TIMEOUT = 1500;
        private const int ARDUINO_TIMEOUT = 1500;

        private bool running_;
        private static GS_Comm instance_;
        private UdpClient GS_Client_;
        private IPEndPoint GC_EndPoint_;
        private Thread mainThread_;
        private IPEndPoint UAV_RaspberryEndPoint_;
        private byte[] networkBuffer_;
        private int networkBufferSize_;
        private string remoteIP_;
        private bool raspberryPiConnected_;
        private bool arduinoConnected_;
        private DateTime lastRaspberryPiUpdate_;
        private DateTime lastArduinoUpdate_;

        public class ArduinoHertzRateArgs : EventArgs
        {
            public MemoryStructs.ArduinoUpdateHertzStruct arduinoHertz_;

            public ArduinoHertzRateArgs(MemoryStructs.ArduinoUpdateHertzStruct arduinoHertz)
            {
                this.arduinoHertz_ = arduinoHertz;
            }
        }

        public class RaspberryPiHertzRateArgs : EventArgs
        {
            public MemoryStructs.RaspberryPiUpdateHertzStruct rasPiUpdateHz_;

            public RaspberryPiHertzRateArgs(MemoryStructs.RaspberryPiUpdateHertzStruct rasPiUpdateHz)
            {
                this.rasPiUpdateHz_ = rasPiUpdateHz;
            }
        }

        public class YawPitchRollArgs : EventArgs
        {
            public float yaw_;
            public float pitch_;
            public float roll_;

            public YawPitchRollArgs(float yaw, float pitch, float roll)
            {
                yaw_ = yaw;
                pitch_ = pitch;
                roll_ = roll;
            }
        }

        public class GpsOutputArgs : EventArgs
        {
            public MemoryStructs.GPS_OutputStruct gpsOutput_;

            public GpsOutputArgs(MemoryStructs.GPS_OutputStruct gpsOutput)
            {
                gpsOutput_ = gpsOutput;
            }
        }

        public class PressureTempAltArgs : EventArgs
        {
            public MemoryStructs.PressureTempAltStruct pressureTempAlt_;

            public PressureTempAltArgs(MemoryStructs.PressureTempAltStruct pressureTempAlt)
            {
                pressureTempAlt_ = pressureTempAlt;
            }
        }

        public class RaltArgs : EventArgs
        {
            public MemoryStructs.RALT_Struct ralt_;

            public RaltArgs(MemoryStructs.RALT_Struct ralt)
            {
                ralt_ = ralt;
            }
        }

        public class VoltageCurrentArgs : EventArgs
        {
            public MemoryStructs.VoltageCurrentStruct voltageCurrent_;

            public VoltageCurrentArgs(MemoryStructs.VoltageCurrentStruct voltageCurrent)
            {
                voltageCurrent_ = voltageCurrent;
            }
        }

        public class HIL_InputArgs : EventArgs
        {
            public MemoryStructs.HIL_InputStruct hilInput_;

            public HIL_InputArgs(MemoryStructs.HIL_InputStruct hilInput)
            {
                hilInput_ = hilInput;
            }
        }

        public class CurrentWaypointArgs : EventArgs
        {
            public MemoryStructs.CurrentWaypointStruct currentWaypoint_;

            public CurrentWaypointArgs(MemoryStructs.CurrentWaypointStruct currentWaypoint)
            {
                currentWaypoint_ = currentWaypoint;
            }
        }

        public class AutopilotModeArgs : EventArgs
        {
            public MemoryStructs.AutoPilotModeStruct autopilotMode_;

            public AutopilotModeArgs(MemoryStructs.AutoPilotModeStruct autopilotMode)
            {
                autopilotMode_ = autopilotMode;
            }
        }

        public class ConsoleArgs : EventArgs
        {
            public MemoryStructs.ConsoleStruct consoleOutput_;

            public ConsoleArgs(MemoryStructs.ConsoleStruct consoleOutput)
            {
                consoleOutput_ = consoleOutput;
            }
        }

        public class WaypointArgs : EventArgs
        {
            public MemoryStructs.WaypointStruct waypoint_;

            public WaypointArgs(MemoryStructs.WaypointStruct waypoint)
            {
                waypoint_ = waypoint;
            }
        }

        public event EventHandler<ArduinoHertzRateArgs> OnArduinoHertzRateReceived;
        public event EventHandler<RaspberryPiHertzRateArgs> OnRaspberryPiHertzRateReceived;
        public event EventHandler<YawPitchRollArgs> OnYawPitchRollReceived;
        public event EventHandler<GpsOutputArgs> OnGpsOutputReceived;
        public event EventHandler<PressureTempAltArgs> OnPressureTempAltReceived;
        public event EventHandler<RaltArgs> OnRaltReceived;
        public event EventHandler<VoltageCurrentArgs> OnVoltageCurrentReceived;
        public event EventHandler<HIL_InputArgs> OnHIL_InputReceived;
        public event EventHandler<CurrentWaypointArgs> OnCurrentWaypointReceived;
        public event EventHandler<AutopilotModeArgs> OnAutopilotModeReceived;
        public event EventHandler<ConsoleArgs> OnConsoleOutputReceived;
        public event EventHandler<WaypointArgs> OnWaypointReceived;
        public event EventHandler OnRaspberryPiConnect;
        public event EventHandler OnRaspberryPiDisconnect;
        public event EventHandler OnArduinoConnect;
        public event EventHandler OnArduinoDisconnect;
        public event EventHandler OnWaypointUploadComplete;
        public event EventHandler OnWaypointUploadReady;
        public event EventHandler OnWaypointDownloadComplete;

        private GS_Comm()
        {
            lastArduinoUpdate_ = DateTime.Now;
            lastRaspberryPiUpdate_ = DateTime.Now;
            raspberryPiConnected_ = false;
            arduinoConnected_ = false;
            running_ = false;
            networkBuffer_ = new byte[NETWORK_BUFFER_SIZE];
            networkBufferSize_ = 0;
        }

        public static GS_Comm GetInstance()
        {
            if (instance_ == null)
                instance_ = new GS_Comm();

            return instance_;
        }

        private void ProcessData(byte[] tempBuffer)
        {
            if (networkBufferSize_ + tempBuffer.Length >= NETWORK_BUFFER_SIZE)
            {
                Console.WriteLine("Error: Memory leak in network buffer\n");
                Stop();
            }
            else
            {
                Array.Copy(tempBuffer, 0, networkBuffer_, networkBufferSize_, tempBuffer.Length);
                networkBufferSize_ += tempBuffer.Length;
            }

            while (networkBufferSize_ >= MemoryStructs.DATA_ID_SIZE)
            {
                MemoryStructs.MemoryStruct_List dataID = (MemoryStructs.MemoryStruct_List)networkBuffer_[0];
                if (dataID == MemoryStructs.MemoryStruct_List.ARDUINO_HERTZ_RATE)
                {
                    if (networkBufferSize_ >= MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.ArduinoUpdateHertzStruct)))
                    {
                        byte[] updateHertzArray = new byte[Marshal.SizeOf(typeof(MemoryStructs.ArduinoUpdateHertzStruct))];
                        Array.Copy(networkBuffer_, MemoryStructs.DATA_ID_SIZE, updateHertzArray, 0, Marshal.SizeOf(typeof(MemoryStructs.ArduinoUpdateHertzStruct)));
                        MemoryStructs.ArduinoUpdateHertzStruct updateHertzStruct = MemoryStructs.ByteArrayToStruct<MemoryStructs.ArduinoUpdateHertzStruct>(updateHertzArray);
                        lastArduinoUpdate_ = DateTime.Now;

                        arduinoConnected_ = true;
                        if (OnArduinoConnect != null)
                            OnArduinoConnect(this, null);                        

                        if (OnArduinoHertzRateReceived != null)
                            OnArduinoHertzRateReceived(this, new ArduinoHertzRateArgs(updateHertzStruct));
                    }

                    //we ate some bytes
                    networkBufferSize_ -= (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.ArduinoUpdateHertzStruct)));
                    //shift data down
                    Array.Copy(networkBuffer_, (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.ArduinoUpdateHertzStruct))), networkBuffer_, 0, networkBufferSize_);
                }
                else if (dataID == MemoryStructs.MemoryStruct_List.RASPBERRY_PI_HERTZ_RATE)
                {
                    if (networkBufferSize_ >= MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.RaspberryPiUpdateHertzStruct)))
                    {
                        byte[] updateHertzArray = new byte[Marshal.SizeOf(typeof(MemoryStructs.RaspberryPiUpdateHertzStruct))];
                        Array.Copy(networkBuffer_, MemoryStructs.DATA_ID_SIZE, updateHertzArray, 0, Marshal.SizeOf(typeof(MemoryStructs.RaspberryPiUpdateHertzStruct)));
                        MemoryStructs.RaspberryPiUpdateHertzStruct updateHertzStruct = MemoryStructs.ByteArrayToStruct<MemoryStructs.RaspberryPiUpdateHertzStruct>(updateHertzArray);
                        lastRaspberryPiUpdate_ = DateTime.Now;

                        raspberryPiConnected_ = true;
                        if (OnRaspberryPiConnect != null)
                            OnRaspberryPiConnect(this, null);                        

                        if (OnRaspberryPiHertzRateReceived != null)
                            OnRaspberryPiHertzRateReceived(this, new RaspberryPiHertzRateArgs(updateHertzStruct));
                    }

                    //we ate some bytes
                    networkBufferSize_ -= (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.RaspberryPiUpdateHertzStruct)));
                    //shift data down
                    Array.Copy(networkBuffer_, (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.RaspberryPiUpdateHertzStruct))), networkBuffer_, 0, networkBufferSize_);
                }
                else if (dataID == MemoryStructs.MemoryStruct_List.YAW_PITCH_ROLL)
                {
                    if (networkBufferSize_ >= MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.YawPitchRollStruct)))
                    {
                        byte[] yawPitchRollArray = new byte[Marshal.SizeOf(typeof(MemoryStructs.YawPitchRollStruct))];
                        Array.Copy(networkBuffer_, MemoryStructs.DATA_ID_SIZE, yawPitchRollArray, 0, Marshal.SizeOf(typeof(MemoryStructs.YawPitchRollStruct)));
                        MemoryStructs.YawPitchRollStruct yawPitchRollStruct = MemoryStructs.ByteArrayToStruct<MemoryStructs.YawPitchRollStruct>(yawPitchRollArray);

                        if (OnYawPitchRollReceived != null)
                            OnYawPitchRollReceived(this, new YawPitchRollArgs(yawPitchRollStruct.yaw, yawPitchRollStruct.pitch, yawPitchRollStruct.roll));
                    }

                    //we ate some bytes
                    networkBufferSize_ -= (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.YawPitchRollStruct)));
                    //shift data down
                    Array.Copy(networkBuffer_, (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.YawPitchRollStruct))), networkBuffer_, 0, networkBufferSize_);
                }
                else if (dataID == MemoryStructs.MemoryStruct_List.GPS_OUTPUT)
                {
                    if (networkBufferSize_ >= MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.GPS_OutputStruct)))
                    {
                        byte[] gpsOutputArray = new byte[Marshal.SizeOf(typeof(MemoryStructs.GPS_OutputStruct))];
                        Array.Copy(networkBuffer_, MemoryStructs.DATA_ID_SIZE, gpsOutputArray, 0, Marshal.SizeOf(typeof(MemoryStructs.GPS_OutputStruct)));
                        MemoryStructs.GPS_OutputStruct gpsOutputStruct = MemoryStructs.ByteArrayToStruct<MemoryStructs.GPS_OutputStruct>(gpsOutputArray);

                        if (OnGpsOutputReceived != null)
                            OnGpsOutputReceived(this, new GpsOutputArgs(gpsOutputStruct));

                        //we ate some bytes
                        networkBufferSize_ -= (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.GPS_OutputStruct)));
                        //shift data down
                        Array.Copy(networkBuffer_, (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.GPS_OutputStruct))), networkBuffer_, 0, networkBufferSize_);
                    }
                    else
                    {
                        //waiting for more data
                        return;
                    }
                }
                else if (dataID == MemoryStructs.MemoryStruct_List.PRESSURE_TEMP_ALT)
                {
                    if (networkBufferSize_ >= MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.PressureTempAltStruct)))
                    {
                        byte[] pressureTempAltArray = new byte[Marshal.SizeOf(typeof(MemoryStructs.PressureTempAltStruct))];
                        Array.Copy(networkBuffer_, MemoryStructs.DATA_ID_SIZE, pressureTempAltArray, 0, Marshal.SizeOf(typeof(MemoryStructs.PressureTempAltStruct)));
                        MemoryStructs.PressureTempAltStruct pressureTempAltStruct = MemoryStructs.ByteArrayToStruct<MemoryStructs.PressureTempAltStruct>(pressureTempAltArray);

                        if (OnPressureTempAltReceived != null)
                            OnPressureTempAltReceived(this, new PressureTempAltArgs(pressureTempAltStruct));

                        //we ate some bytes
                        networkBufferSize_ -= (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.PressureTempAltStruct)));
                        //shift data down
                        Array.Copy(networkBuffer_, (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.PressureTempAltStruct))), networkBuffer_, 0, networkBufferSize_);
                    }
                    else
                    {
                        //waiting for more data
                        return;
                    }
                }
                else if (dataID == MemoryStructs.MemoryStruct_List.RALT)
                {
                    if (networkBufferSize_ >= MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.RALT_Struct)))
                    {
                        byte[] raltArray = new byte[Marshal.SizeOf(typeof(MemoryStructs.RALT_Struct))];
                        Array.Copy(networkBuffer_, MemoryStructs.DATA_ID_SIZE, raltArray, 0, Marshal.SizeOf(typeof(MemoryStructs.RALT_Struct)));
                        MemoryStructs.RALT_Struct raltStruct = MemoryStructs.ByteArrayToStruct<MemoryStructs.RALT_Struct>(raltArray);

                        if (OnRaltReceived != null)
                            OnRaltReceived(this, new RaltArgs(raltStruct));

                        //we ate some bytes
                        networkBufferSize_ -= (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.RALT_Struct)));
                        //shift data down
                        Array.Copy(networkBuffer_, (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.RALT_Struct))), networkBuffer_, 0, networkBufferSize_);
                    }
                    else
                    {
                        //waiting for more data
                        return;
                    }
                }
                else if (dataID == MemoryStructs.MemoryStruct_List.VOLTAGE_CURRENT)
                {
                    if (networkBufferSize_ >= MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.VoltageCurrentStruct)))
                    {
                        byte[] voltageCurrentArray = new byte[Marshal.SizeOf(typeof(MemoryStructs.VoltageCurrentStruct))];
                        Array.Copy(networkBuffer_, MemoryStructs.DATA_ID_SIZE, voltageCurrentArray, 0, Marshal.SizeOf(typeof(MemoryStructs.VoltageCurrentStruct)));
                        MemoryStructs.VoltageCurrentStruct voltageCurrentStructStruct = MemoryStructs.ByteArrayToStruct<MemoryStructs.VoltageCurrentStruct>(voltageCurrentArray);                        

                        if (OnVoltageCurrentReceived != null)
                            OnVoltageCurrentReceived(this, new VoltageCurrentArgs(voltageCurrentStructStruct));

                        //we ate some bytes
                        networkBufferSize_ -= (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.VoltageCurrentStruct)));
                        //shift data down
                        Array.Copy(networkBuffer_, (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.VoltageCurrentStruct))), networkBuffer_, 0, networkBufferSize_);
                    }
                    else
                    {
                        //waiting for more data
                        return;
                    }
                }
                else if (dataID == MemoryStructs.MemoryStruct_List.WAYPOINT_UPLOAD_READY)
                {
                    if (OnWaypointUploadReady != null)
                        OnWaypointUploadReady(this, null);

                    //we ate some bytes
                    networkBufferSize_ -= MemoryStructs.DATA_ID_SIZE;
                    //shift data down
                    Array.Copy(networkBuffer_, MemoryStructs.DATA_ID_SIZE, networkBuffer_, 0, networkBufferSize_);
                }
                else if (dataID == MemoryStructs.MemoryStruct_List.WAYPOINT_UPLOAD_COMPLETE)
                {
                    if (OnWaypointUploadComplete != null)
                        OnWaypointUploadComplete(this, null);

                    //we ate some bytes
                    networkBufferSize_ -= MemoryStructs.DATA_ID_SIZE;
                    //shift data down
                    Array.Copy(networkBuffer_, MemoryStructs.DATA_ID_SIZE, networkBuffer_, 0, networkBufferSize_);
                }
                else if (dataID == MemoryStructs.MemoryStruct_List.HIL_INPUT)
                {
                    if (networkBufferSize_ >= MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.HIL_InputStruct)))
                    {
                        byte[] hilInputStructArray = new byte[Marshal.SizeOf(typeof(MemoryStructs.HIL_InputStruct))];
                        Array.Copy(networkBuffer_, MemoryStructs.DATA_ID_SIZE, hilInputStructArray, 0, Marshal.SizeOf(typeof(MemoryStructs.HIL_InputStruct)));
                        MemoryStructs.HIL_InputStruct hilInputStruct = MemoryStructs.ByteArrayToStruct<MemoryStructs.HIL_InputStruct>(hilInputStructArray);

                        if (OnHIL_InputReceived != null)
                            OnHIL_InputReceived(this, new HIL_InputArgs(hilInputStruct));

                        //we ate some bytes
                        networkBufferSize_ -= (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.HIL_InputStruct)));
                        //shift data down
                        Array.Copy(networkBuffer_, (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.HIL_InputStruct))), networkBuffer_, 0, networkBufferSize_);
                    }
                    else
                    {
                        //waiting for more data
                        return;
                    }
                }
                else if (dataID == MemoryStructs.MemoryStruct_List.GET_CURRENT_WAYPOINT)
                {
                    if (networkBufferSize_ >= MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.CurrentWaypointStruct)))
                    {
                        byte[] currentWaypointStructArray = new byte[Marshal.SizeOf(typeof(MemoryStructs.CurrentWaypointStruct))];
                        Array.Copy(networkBuffer_, MemoryStructs.DATA_ID_SIZE, currentWaypointStructArray, 0, Marshal.SizeOf(typeof(MemoryStructs.CurrentWaypointStruct)));
                        MemoryStructs.CurrentWaypointStruct currentWaypointStruct = MemoryStructs.ByteArrayToStruct<MemoryStructs.CurrentWaypointStruct>(currentWaypointStructArray);

                        if (OnCurrentWaypointReceived != null)
                            OnCurrentWaypointReceived(this, new CurrentWaypointArgs(currentWaypointStruct));

                        //we ate some bytes
                        networkBufferSize_ -= (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.CurrentWaypointStruct)));
                        //shift data down
                        Array.Copy(networkBuffer_, (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.CurrentWaypointStruct))), networkBuffer_, 0, networkBufferSize_);
                    }
                    else
                    {
                        //waiting for more data
                        return;
                    }
                }
                else if (dataID == MemoryStructs.MemoryStruct_List.CURRENT_AUTOPILOT_MODE)
                {
                    if (networkBufferSize_ >= MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.AutoPilotModeStruct)))
                    {
                        byte[] autoPilotModeStructArray = new byte[Marshal.SizeOf(typeof(MemoryStructs.AutoPilotModeStruct))];
                        Array.Copy(networkBuffer_, MemoryStructs.DATA_ID_SIZE, autoPilotModeStructArray, 0, Marshal.SizeOf(typeof(MemoryStructs.AutoPilotModeStruct)));
                        MemoryStructs.AutoPilotModeStruct autoPilotModeStruct = MemoryStructs.ByteArrayToStruct<MemoryStructs.AutoPilotModeStruct>(autoPilotModeStructArray);

                        if (OnAutopilotModeReceived != null)
                            OnAutopilotModeReceived(this, new AutopilotModeArgs(autoPilotModeStruct));

                        //we ate some bytes
                        networkBufferSize_ -= (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.AutoPilotModeStruct)));
                        //shift data down
                        Array.Copy(networkBuffer_, (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.AutoPilotModeStruct))), networkBuffer_, 0, networkBufferSize_);
                    }
                    else
                    {
                        //waiting for more data
                        return;
                    }
                }
                else if (dataID == MemoryStructs.MemoryStruct_List.CONSOLE_OUT)
                {
                    if (networkBufferSize_ >= MemoryStructs.DATA_ID_SIZE + MemoryStructs.CONSOLE_SIZE)
                    {
                        byte[] consoleStructArray = new byte[MemoryStructs.CONSOLE_SIZE];
                        Array.Copy(networkBuffer_, MemoryStructs.DATA_ID_SIZE, consoleStructArray, 0, MemoryStructs.CONSOLE_SIZE);
                        MemoryStructs.ConsoleStruct consoleStruct = new MemoryStructs.ConsoleStruct();
                        consoleStruct.console = new char[MemoryStructs.CONSOLE_SIZE];
                        for (int i = 0; i < MemoryStructs.CONSOLE_SIZE; i++)
                        {
                            if ((char)consoleStructArray[i] == '\0')
                            {
                                Array.Resize<char>(ref consoleStruct.console, i);
                                break;
                            }
                            consoleStruct.console[i] = (char)consoleStructArray[i];
                        }

                        if (OnConsoleOutputReceived != null)
                            OnConsoleOutputReceived(this, new ConsoleArgs(consoleStruct));

                        //we ate some bytes
                        networkBufferSize_ -= (MemoryStructs.DATA_ID_SIZE + MemoryStructs.CONSOLE_SIZE);
                        //shift data down
                        Array.Copy(networkBuffer_, (MemoryStructs.DATA_ID_SIZE + MemoryStructs.CONSOLE_SIZE), networkBuffer_, 0, networkBufferSize_);
                    }
                    else
                    {
                        //waiting for more data
                        return;
                    }
                }
                else if (dataID == MemoryStructs.MemoryStruct_List.WAYPOINT)
                {
                    if (networkBufferSize_ >= MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.WaypointStruct)))
                    {
                        byte[] WaypointStructArray = new byte[Marshal.SizeOf(typeof(MemoryStructs.WaypointStruct))];
                        Array.Copy(networkBuffer_, MemoryStructs.DATA_ID_SIZE, WaypointStructArray, 0, Marshal.SizeOf(typeof(MemoryStructs.WaypointStruct)));
                        MemoryStructs.WaypointStruct waypointStruct = MemoryStructs.ByteArrayToStruct<MemoryStructs.WaypointStruct>(WaypointStructArray);

                        if (OnWaypointReceived != null)
                            OnWaypointReceived(this, new WaypointArgs(waypointStruct));

                        //we ate some bytes
                        networkBufferSize_ -= (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.WaypointStruct)));
                        //shift data down
                        Array.Copy(networkBuffer_, (MemoryStructs.DATA_ID_SIZE + Marshal.SizeOf(typeof(MemoryStructs.WaypointStruct))), networkBuffer_, 0, networkBufferSize_);
                    }
                    else
                    {
                        //waiting for more data
                        return;
                    }
                }
                else if (dataID == MemoryStructs.MemoryStruct_List.WAYPOINT_DOWNLOAD_COMPLETE)
                {
                    if (networkBufferSize_ >= MemoryStructs.DATA_ID_SIZE)
                    {
                        if (OnWaypointDownloadComplete != null)
                            OnWaypointDownloadComplete(this, null);

                        //we ate some bytes
                        networkBufferSize_ -= MemoryStructs.DATA_ID_SIZE;
                        //shift data down
                        Array.Copy(networkBuffer_, MemoryStructs.DATA_ID_SIZE, networkBuffer_, 0, networkBufferSize_);
                    }
                    else
                    {
                        //waiting for more data
                        return;
                    }
                }
                else
                {
                    Console.WriteLine("Error bad network data ID\n");
                    Stop();
                }
            }
        }

        private void Update()
        {
            while (running_)
            {
                try
                {
                    if (GS_Client_.Available > 0)
                    {
                        byte[] tempBuffer = GS_Client_.Receive(ref GC_EndPoint_);

                        if (tempBuffer.Length > 0)
                        {
                            ProcessData(tempBuffer);
                        }
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine("Error reading buffer");
                    Stop();
                }

                if (raspberryPiConnected_)
                {
                    if ((DateTime.Now - lastRaspberryPiUpdate_).TotalMilliseconds > RASPBERRY_PI_TIMEOUT)
                    {
                        if (OnRaspberryPiDisconnect != null)
                            OnRaspberryPiDisconnect(this, null);
                    }
                }

                if (arduinoConnected_)
                {
                    if ((DateTime.Now - lastArduinoUpdate_).TotalMilliseconds > ARDUINO_TIMEOUT)
                    {                        
                        if (OnArduinoDisconnect != null)
                            OnArduinoDisconnect(this, null);                        
                    }
                }

                Thread.Sleep(GS_COMM_UPDATE_HERTZ);
            }
        }

        public string GetRemoteIP()
        {
            return remoteIP_;
        }

        public bool IsRunning()
        {
            return running_;
        }

        public bool IsRaspberryPiConnecting()
        {
            return running_;
        }

        public bool IsRaspberryPiConnected()
        {
            return raspberryPiConnected_;
        }

        public bool IsArduinoConnected()
        {
            return arduinoConnected_;
        }

        public void Start(string UavIpAddress)
        {
            try
            {
                running_ = true;
                GC_EndPoint_ = new IPEndPoint(IPAddress.Any, LISTEN_PORT);
                remoteIP_ = UavIpAddress;
                UAV_RaspberryEndPoint_ = new IPEndPoint(IPAddress.Parse(UavIpAddress), SEND_PORT);
                GS_Client_ = new UdpClient(GC_EndPoint_);                                
                mainThread_ = new Thread(Update);
                //mainThread_.SetApartmentState(ApartmentState.STA);
                mainThread_.Start();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void Stop()
        {            
            CloseConnection();

            if (mainThread_ != null)
                mainThread_.Join(100);
            running_ = false;
        }

        private void CloseConnection()
        {
            if (GS_Client_ != null)
                GS_Client_.Close();
        }

        public bool SendData(MemoryStructs.MemoryStruct_List dataID, byte[] data)
        {
            if (GS_Client_ != null && running_ && (arduinoConnected_ || dataID == MemoryStructs.MemoryStruct_List.CONNECTION_INFO))
            {
                try
                {
                    byte[] buffer = new byte[MemoryStructs.DATA_ID_SIZE + data.Length];
                    byte[] convertedDataID = { (byte)dataID };
                    convertedDataID.CopyTo(buffer, 0);
                    data.CopyTo(buffer, MemoryStructs.DATA_ID_SIZE);
                    GS_Client_.Send(buffer, buffer.Length, UAV_RaspberryEndPoint_);
                    return true;
                }
                catch
                {
                    CloseConnection();
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
