﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace GroundStation
{
    /// <summary>
    /// Contains all the messages need to be sent over the network via IP and sent over serial to the arduino.
    /// This file is taylored for C#, the version of memoryStructs used by the Pi and Arduino are identical.
    /// </summary>
    /// <remarks>
    /// longs in C++ on Win & Linux & Arduino are 4 bytes, where as they are 8 in C#
    /// ints on the Arduino are 2 bytes where as they are 4 on the Pi and Windows
    /// Max message size that I can send to the arduino seems to about around 13 bytes, but I think I can receive more.    
    /// </remarks>
    public class MemoryStructs
    {
        public const int DATA_ID_SIZE = 1;
        public const int CONSOLE_SIZE = 24;

        public enum WaypointTypeEnum { TAKEOFF, LAND, WAYPOINT, GOTO }

        public enum MemoryStruct_List
        {
            START_STOP_VIDEO,
            TEST_LIGHT,
            ARDUINO_HERTZ_RATE,
            RASPBERRY_PI_HERTZ_RATE,
            YAW_PITCH_ROLL,
            ARDUINO_PING,
            AILERON_ELEVATOR_INPUT,
            RUDDER_INPUT,
            THROTTLE_INPUT,
            FLAPS_INPUT,
            ENGINE_STATUS_INPUT,
            CONNECTION_INFO,
            GPS_OUTPUT,
            HIL_SETTINGS,
            HIL_OUTPUT,
            HIL_INPUT,
            PRESSURE_TEMP_ALT,
            RALT,
            VOLTAGE_CURRENT,
            WAYPOINT,
            START_WAYPOINT_UPLOAD,
            WAYPOINT_UPLOAD_READY,
            WAYPOINT_UPLOAD_COMPLETE,
            REQUEST_AUTOPILOT_MODE,
            CURRENT_AUTOPILOT_MODE,
            SET_CURRENT_WAYPOINT,
            GET_CURRENT_WAYPOINT,
            GET_GOTO_ITERATIONS,
            CONSOLE_OUT,
            WAYPOINT_DOWNLOAD_ROUTE,
            WAYPOINT_DOWNLOAD_COMPLETE
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ConsoleStruct
        {
            public char[] console;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct GotoIterationsStruct
        {
            public byte waypointIndex;
            public byte iterationsLeft;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CurrentWaypointStruct
        {
            public byte waypointIndex;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct AutoPilotModeStruct
        {
            [MarshalAs(UnmanagedType.I1)]
            public bool autopilotEnabled;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct StartWaypointUploadStruct
        {
            public byte waypointCount;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct WaypointStruct
        {
            public float altitude;
            public float heading;
            public float knots;
            public float latitude;
            public float longitude;
            public byte waypointIndex;
            public byte waypointID;
            public byte waypointType;
            public byte goToIndex;
            public byte iterations;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct VoltageCurrentStruct
        {
            public float voltage;
            public float current;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct RALT_Struct
        {
            public float altitude;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PressureTempAltStruct
        {
            public int pressure; //pa 
            public float altitude; //meters MSL
            public short temperature; //celsius x10
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct HIL_SettingsStruct
        {
            [MarshalAs(UnmanagedType.I1)]
            public bool hilEnabled;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct HIL_OutputStruct
        {
            public float latitude;
            public float longitude;
            public float altitude; //MSL feet   
            public float RALT;
            public float airspeed; //kt   
            public float yaw;
            public float pitch;
            public float roll;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct HIL_InputStruct
        {
            public float flaps;
            public float rudder;
            public float ailerons;
            public float elevators;
            public float throttle;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct GPS_OutputStruct
        {            
	        public float longitude;
	        public float latitude;	
	        public float altitude;
	        public float knots;          
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ConnectionInfoStruct
        {
            public byte firstOctet;
            public byte secondOctet;
            public byte thirdOctet;
            public byte fourthOctet;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct RaspberryPiUpdateHertzStruct
        {
            public float updateRate;
            public float signalStrength;
            public int txBps;
            public int rxBps;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ArduinoUpdateHertzStruct
        {
            public float updateRate;
            public short availableMemory;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct YawPitchRollStruct
        {
            public float yaw;
            public float pitch;
            public float roll;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct StartStopVideoStruct
        {
            [MarshalAs(UnmanagedType.I1)]
            public bool start;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TestLightStruct
        {
            [MarshalAs(UnmanagedType.I1)]
            public bool on;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct AileronElevatorInputStruct
        {
            public float aileron;
            public float elevator;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct RudderInputStruct
        {
            public float rudder;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ThrottleInputStruct
        {
            public float throttle;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct FlapsInputStruct
        {
            [MarshalAs(UnmanagedType.I1)]
            public bool flapsDown;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct EngineStatusInputStruct
        {
            [MarshalAs(UnmanagedType.I1)]
            public bool engineOn;
        }

        static public T ByteArrayToStruct<T>(byte[] bytes) where T : struct
        {
            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            T stuff = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(),
                typeof(T));
            handle.Free();
            return stuff;
        }

        static public byte[] StructToByteArray<T>(T structure) where T : struct
        {
            int size = Marshal.SizeOf(structure);
            byte[] arr = new byte[size];
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.StructureToPtr(structure, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);

            return arr;
        }
    }
}
