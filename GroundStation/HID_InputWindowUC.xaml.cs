﻿using GroundStation.HID_Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GroundStation
{
    /// <summary>
    /// Interaction logic for HID_InputWindowUC.xaml
    /// </summary>
    public partial class HID_InputWindowUC : UserControl
    {
        public HID_InputWindowUC()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            SetStatus();
            XBoxInput.GetInstance().OnAileronElevatorInputChanged += HID_InputWindow_OnAileronElevatorInputChanged;
            XBoxInput.GetInstance().OnRudderInputChanged += HID_InputWindow_OnRudderInputChanged;
            XBoxInput.GetInstance().OnThrottleInputChanged += HID_InputWindow_OnThrottleInputChanged;
            XBoxInput.GetInstance().OnFlapsStatusInputChanged += HID_InputWindow_OnFlapsStatusInputChanged;
            XBoxInput.GetInstance().OnEngineStatusInputChanged += HID_InputWindow_OnEngineStatusInputChanged;
            XBoxInput.GetInstance().OnJoystickConnectionChanged += HID_InputWindow_OnJoystickConnectionChanged;
            XBoxInput.GetInstance().OnAutopilotInputChanged += HID_InputWindowUC_OnAutopilotInputChanged;
        }

        void HID_InputWindowUC_OnAutopilotInputChanged(object sender, HID_Input.HID_Input.AutopilotInputArgs e)
        {
            SetStatus();
        }

        void HID_InputWindow_OnEngineStatusInputChanged(object sender, HID_Input.HID_Input.EngineStatusInputArgs e)
        {
            SetStatus();
        }

        void HID_InputWindow_OnFlapsStatusInputChanged(object sender, HID_Input.HID_Input.FlapsStatusInputArgs e)
        {
            SetStatus();
        }

        void HID_InputWindow_OnThrottleInputChanged(object sender, HID_Input.HID_Input.ThrottleInputArgs e)
        {
            SetStatus();
        }

        void HID_InputWindow_OnRudderInputChanged(object sender, HID_Input.HID_Input.RudderInputArgs e)
        {
            SetStatus();
        }

        void HID_InputWindow_OnJoystickConnectionChanged(object sender, HID_Input.HID_Input.JoystickConnectionChangedArgs e)
        {
            SetStatus();
        }

        void HID_InputWindow_OnAileronElevatorInputChanged(object sender, HID_Input.HID_Input.AileronElevatorInputArgs e)
        {
            SetStatus();
        }

        void SetStatus()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                lblConnection.Content = XBoxInput.GetInstance().JoystickConnected ? "Joystick Connected" : "Joystick Not Connected";
                lblAileron.Content = "Ailerons: " + XBoxInput.GetInstance().Ailerons.ToString("P2");
                lblElevator.Content = "Elevators: " + XBoxInput.GetInstance().Elevators.ToString("P2");
                lblRudder.Content = "Rudder: " + XBoxInput.GetInstance().Rudders.ToString("P2");
                lblThrottle.Content = "Throttle: " + XBoxInput.GetInstance().Throttle.ToString("P2");
                lblFlaps.Content = "Flaps: " + (XBoxInput.GetInstance().FlapsDown ? "Down" : "Up");
                lblEngineOn.Content = "Engine: " + (XBoxInput.GetInstance().EngineOn ? "On" : "Off");
                lblAP_Mode.Content = "Requected AP Mode: " + (XBoxInput.GetInstance().AutopilotOn ? "On" : "Off");
            }));

        }

        private void UserControl_Unloaded_1(object sender, RoutedEventArgs e)
        {
            XBoxInput.GetInstance().OnAileronElevatorInputChanged -= HID_InputWindow_OnAileronElevatorInputChanged;
            XBoxInput.GetInstance().OnRudderInputChanged -= HID_InputWindow_OnRudderInputChanged;
            XBoxInput.GetInstance().OnThrottleInputChanged -= HID_InputWindow_OnThrottleInputChanged;
            XBoxInput.GetInstance().OnFlapsStatusInputChanged -= HID_InputWindow_OnFlapsStatusInputChanged;
            XBoxInput.GetInstance().OnEngineStatusInputChanged -= HID_InputWindow_OnEngineStatusInputChanged;
            XBoxInput.GetInstance().OnJoystickConnectionChanged -= HID_InputWindow_OnJoystickConnectionChanged;
            XBoxInput.GetInstance().OnAutopilotInputChanged -= HID_InputWindowUC_OnAutopilotInputChanged;
        }
    }
}
