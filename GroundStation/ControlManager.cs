﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using GroundStation.HID_Input;

namespace GroundStation
{
    class ControlManager
    {
        private const int CONTROL_MANAGER_REFRESH_RATE = 450;
        private const int CONTROL_MANAGER_SEND_RATE = 66;
        private const int CONTROL_MANAGER_UPDATE_HERTZ = 1;

        private Mutex controlManagerMutex_;
        private bool running_;
        private Thread mainThread_;
        private DateTime lastSend_;
        private DateTime lastRefresh_;
        private static ControlManager instance_;

        MemoryStructs.AileronElevatorInputStruct aileronElevatorInput_;
        private bool sendAileronElevator_;
        MemoryStructs.EngineStatusInputStruct engineStatusInput_;
        private bool sendEngineStatus_;
        MemoryStructs.FlapsInputStruct flapsStatusInput_;
        private bool sendFlaps_;
        MemoryStructs.RudderInputStruct rudderInput_;
        private bool sendRudder_;
        MemoryStructs.ThrottleInputStruct throttleInput_;
        private bool sendThrottle_;

        public static ControlManager GetInstance()
        {
            if (instance_ == null)
                instance_ = new ControlManager();

            return instance_;
        }

        private ControlManager()
        {
            aileronElevatorInput_ = new MemoryStructs.AileronElevatorInputStruct();
            engineStatusInput_ = new MemoryStructs.EngineStatusInputStruct();
            flapsStatusInput_ = new MemoryStructs.FlapsInputStruct();
            rudderInput_ = new MemoryStructs.RudderInputStruct();
            throttleInput_ = new MemoryStructs.ThrottleInputStruct();

            sendAileronElevator_ = false;
            sendEngineStatus_ = false;
            sendFlaps_ = false;
            sendRudder_ = false;
            sendThrottle_ = false;

            controlManagerMutex_ = new Mutex();
            lastSend_ = DateTime.Now;
            lastRefresh_ = DateTime.Now;

            XBoxInput.GetInstance().OnAileronElevatorInputChanged += ControlManager_OnAileronElevatorInputChanged;
            XBoxInput.GetInstance().OnEngineStatusInputChanged += ControlManager_OnEngineStatusInputChanged;
            XBoxInput.GetInstance().OnFlapsStatusInputChanged += ControlManager_OnFlapsStatusInputChanged;
            XBoxInput.GetInstance().OnRudderInputChanged += ControlManager_OnRudderInputChanged;
            XBoxInput.GetInstance().OnThrottleInputChanged += ControlManager_OnThrottleInputChanged;            
        }

        public void Start()
        {
            running_ = true;
            mainThread_ = new Thread(Update);
            mainThread_.Start();
        }

        public void Stop()
        {
            running_ = false;
            if (mainThread_ != null)
                mainThread_.Join(100);
        }

        private void Update()
        {
            while (running_)
            {
                if ((DateTime.Now - lastSend_).TotalMilliseconds >= CONTROL_MANAGER_SEND_RATE)
                {
                    controlManagerMutex_.WaitOne();

                    if ((DateTime.Now - lastRefresh_).TotalMilliseconds >= CONTROL_MANAGER_REFRESH_RATE)
                    {
                        sendAileronElevator_ = true;
                        sendEngineStatus_ = true;
                        sendFlaps_ = true;
                        sendRudder_ = true;
                        sendThrottle_ = true;
                        lastRefresh_ = DateTime.Now;
                    }

                    if (sendAileronElevator_)
                        GS_Comm.GetInstance().SendData(MemoryStructs.MemoryStruct_List.AILERON_ELEVATOR_INPUT, MemoryStructs.StructToByteArray<MemoryStructs.AileronElevatorInputStruct>(aileronElevatorInput_));

                    if (sendEngineStatus_)
                        GS_Comm.GetInstance().SendData(MemoryStructs.MemoryStruct_List.ENGINE_STATUS_INPUT, MemoryStructs.StructToByteArray<MemoryStructs.EngineStatusInputStruct>(engineStatusInput_));

                    if (sendFlaps_)
                        GS_Comm.GetInstance().SendData(MemoryStructs.MemoryStruct_List.FLAPS_INPUT, MemoryStructs.StructToByteArray<MemoryStructs.FlapsInputStruct>(flapsStatusInput_));

                    if (sendRudder_)
                        GS_Comm.GetInstance().SendData(MemoryStructs.MemoryStruct_List.RUDDER_INPUT, MemoryStructs.StructToByteArray<MemoryStructs.RudderInputStruct>(rudderInput_));

                    if (sendThrottle_)
                        GS_Comm.GetInstance().SendData(MemoryStructs.MemoryStruct_List.THROTTLE_INPUT, MemoryStructs.StructToByteArray<MemoryStructs.ThrottleInputStruct>(throttleInput_));

                    sendAileronElevator_ = false;
                    sendEngineStatus_ = false;
                    sendFlaps_ = false;
                    sendRudder_ = false;
                    sendThrottle_ = false;

                    controlManagerMutex_.ReleaseMutex();
                    lastSend_ = DateTime.Now;
                }

                Thread.Sleep(CONTROL_MANAGER_UPDATE_HERTZ);
            }
        }

        void ControlManager_OnAileronElevatorInputChanged(object sender, HID_Input.HID_Input.AileronElevatorInputArgs e)
        {
            controlManagerMutex_.WaitOne();
            aileronElevatorInput_.aileron = e.aileron_;
            aileronElevatorInput_.elevator = e.elevator_;
            sendAileronElevator_ = true;
            controlManagerMutex_.ReleaseMutex();
        }

        void ControlManager_OnEngineStatusInputChanged(object sender, HID_Input.HID_Input.EngineStatusInputArgs e)
        {
            controlManagerMutex_.WaitOne();
            engineStatusInput_.engineOn = e.engineOn_;
            sendEngineStatus_ = true;
            controlManagerMutex_.ReleaseMutex();
        }

        void ControlManager_OnFlapsStatusInputChanged(object sender, HID_Input.HID_Input.FlapsStatusInputArgs e)
        {
            controlManagerMutex_.WaitOne();
            flapsStatusInput_.flapsDown = e.flapsDown_;
            sendFlaps_ = true;
            controlManagerMutex_.ReleaseMutex();
        }

        void ControlManager_OnRudderInputChanged(object sender, HID_Input.HID_Input.RudderInputArgs e)
        {
            controlManagerMutex_.WaitOne();
            rudderInput_.rudder = e.rudder_;
            sendRudder_ = true;
            controlManagerMutex_.ReleaseMutex();
        }

        void ControlManager_OnThrottleInputChanged(object sender, HID_Input.HID_Input.ThrottleInputArgs e)
        {
            controlManagerMutex_.WaitOne();
            throttleInput_.throttle = e.throttle_;
            sendThrottle_ = true;
            controlManagerMutex_.ReleaseMutex();
        }
    }
}
