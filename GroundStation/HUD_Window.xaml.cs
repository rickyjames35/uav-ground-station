﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GroundStation
{
    /// <summary>
    /// Interaction logic for HUD_Window.xaml
    /// </summary>
    public partial class HUD_Window : Window
    {
        private const double pitchRollScale = 0.6F;
        private const double headingHeightScale = 0.1F;
        private const double headingWidthScale = 0.5F;
        private const double noseVectorScale = 0.05F;
        private const double screenRatio = 1024F / 768F;

        public HUD_Window()
        {
            InitializeComponent();
        }

        private void Window_SizeChanged_1(object sender, SizeChangedEventArgs e)
        {
            double videoHeight;
            double videoWidth;
            double videoTop;
            double videoLeft;

            //width wins
            if (mainCanvas.ActualWidth / mainCanvas.ActualHeight < screenRatio)
            {
                videoWidth = mainCanvas.ActualWidth;
                videoHeight = mainCanvas.ActualWidth * (1 / screenRatio);
                videoLeft = 0;
                videoTop = (mainCanvas.ActualHeight - videoHeight) / 2;
            }
            //height wins
            else
            {
                videoHeight = mainCanvas.ActualHeight;
                videoWidth = mainCanvas.ActualHeight * screenRatio;
                videoTop = 0;
                videoLeft = (mainCanvas.ActualWidth - videoWidth) / 2;
            }

            noseVector.Width = videoWidth * noseVectorScale;
            noseVector.Height = noseVector.Width;
            noseVector.Margin = new Thickness(videoLeft + (videoWidth / 2) - (noseVector.Width / 2), videoTop + (videoHeight / 2) - (noseVector.Height / 2), 0, 0);

            heading.Width = videoWidth * headingWidthScale;
            heading.Height = videoHeight * headingHeightScale;
            heading.Margin = new Thickness(videoLeft + (videoWidth / 2) - (heading.Width / 2), videoTop, 0, 0);

            pitchRoll.Width = videoWidth * pitchRollScale;
            pitchRoll.Height = pitchRoll.Width;
            pitchRoll.Margin = new Thickness(videoLeft + (videoWidth / 2) - (pitchRoll.Width / 2), videoTop + (videoHeight / 2) - (pitchRoll.Height / 2), 0, 0);
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            GS_Comm.GetInstance().OnYawPitchRollReceived += HUD_Window_OnYawPitchRollReceived;
        }

        void HUD_Window_OnYawPitchRollReceived(object sender, GS_Comm.YawPitchRollArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() => {
                heading.SetHeading(e.yaw_);                
                pitchRoll.SetPitchRoll(e.pitch_, e.roll_);
            }));
        }
    }
}
